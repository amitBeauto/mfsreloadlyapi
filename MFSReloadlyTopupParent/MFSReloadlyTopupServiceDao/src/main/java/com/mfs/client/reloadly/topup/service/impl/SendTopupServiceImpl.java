package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ReciepientPhone;
import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto1;
import com.mfs.client.reloadly.topup.dto.SendTopupResponseDto;
import com.mfs.client.reloadly.topup.dto.SenderPhone;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionErrorModel;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionLogModel;
import com.mfs.client.reloadly.topup.service.SendTopupService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;
import com.mfs.client.reloadly.topup.util.TimeDelayCheck;

/**
 * @ author Mohsin Shaikh SendTopupServiceImpl.java 30-Jan-2020
 * 
 * purpose : this class is used for send topup
 * 
 * Methods 1.sendTopup Methods 2.logSendTopupRequest Methods 3.logResponse
 * Methods 4.logError
 * 
 * Change_history 1 . Added Fields : operatorTransactionId , discount ,
 * pinDetail , operatorName , discountCurrencyCode in responseDto and Model
 * Class.
 * 
 */
@Service("SendTopupService")
public class SendTopupServiceImpl implements SendTopupService {

	private static final Logger LOGGER = Logger.getLogger(SendTopupServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	@Autowired
	TimeDelayCheck timeDelayCheck;

	// Prepare SendTopUp service to call endpoint Url and getting response
	public SendTopupResponseDto sendTopup(SendTopupRequestDto requestDto) {

		SendTopupResponseDto response = new SendTopupResponseDto();
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> systemConfigDetails = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Gson gson = new Gson();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		
		try {
			// Get Last Record Details of Access Token
			ReloadlyAuthorizationModel reloadlyAuthorizationModel = transactionDao.getLastRecordByAccessTokenId();
			if (reloadlyAuthorizationModel == null) {
				response.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				response.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = reloadlyAuthorizationModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {

					// Send topup request for reloadly
					SendTopupRequestDto1 requestDto1 = new SendTopupRequestDto1();
					ReciepientPhone phone = new ReciepientPhone();
					phone.setCountryCode(requestDto.getRecipientPhone().getCountryCode());
					phone.setNumber(requestDto.getRecipientPhone().getNumber());
					requestDto1.setRecipientPhone(phone);
					SenderPhone phone1 = new SenderPhone();
					phone1.setCountryCode(requestDto.getSenderPhone().getCountryCode());
					phone1.setNumber(requestDto.getSenderPhone().getNumber());
					requestDto1.setSenderPhone(phone1);
					requestDto1.setOperatorId(requestDto.getOperatorId());
					requestDto1.setAmount(requestDto.getAmount());
					requestDto1.setCustomIdentifier(requestDto.getCustomIdentifier());
					LOGGER.info("SendTopupServiceImpl in sendTopup function " + requestDto1);

					// Get transaction log record against mfsTransid to check duplicate record
					ReloadlyTransactionLogModel reloadlyTransactionLogModel = transactionDao
							.getSendtopupByMfsTransId(requestDto.getMfsTransId());
					if (reloadlyTransactionLogModel == null) {

						// get System Config Details from DB
						systemConfigDetails = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
						if (systemConfigDetails == null) {
							throw new Exception("No config Details Found");
						}

						// Set Header Parameters
						Map<String, String> headerMap = new HashMap<String, String>();
						headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
						headerMap.put(CommonConstant.AUTHORIZATION,
								CommonConstant.BEARER + reloadlyAuthorizationModel.getAccessToken());
						headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
						connectionRequest.setHeaders(headerMap);
						connectionRequest.setHttpmethodName("POST");
						connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));

						// Set URL for SendTopup
						String url = (systemConfigDetails.get(CommonConstant.BASE_URL)
								+ systemConfigDetails.get(CommonConstant.TOPUPS));
						connectionRequest.setServiceUrl(url);

						// Get Last Record detail from transaction log detail
						List<ReloadlyTransactionLogModel> reloadlyTransactionLogModel1 = transactionDao
								.getLastRecordtransactionLogId(requestDto.getRecipientPhone().getNumber(),
										requestDto.getSenderPhone().getNumber());
						boolean timeDealay = false;
						// Check reloadlyTransactionLogModel empty or not
						if ((reloadlyTransactionLogModel1.size() != 0)) {

							// Check for delay of 2 minutes
							timeDealay = timeDelayCheck.timeDelay(reloadlyTransactionLogModel1.get(0),
									systemConfigDetails);

							if (!timeDealay) {
								// Log Sendtopup Request
								logSendTopupRequest(requestDto);

								httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(gson.toJson(requestDto1),
										connectionRequest);
								String str_result = httpsConResult.getResponseData();
								String status = httpsConResult.getTxMessage();

								// Check Response Code recieved in response is Success or not
								if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
									// Convert response from Json to Object
									response = (SendTopupResponseDto) JSONToObjectConversion
											.getObjectFromJson(str_result, SendTopupResponseDto.class);
									LOGGER.info("SendTopupServiceImpl in sendTopup function " + response);
									// Log Response for sendtopup
									logResponse(response, requestDto, status);
								} else {
									logError(httpsConResult.getCode(), status, requestDto);
									response.setCode(httpsConResult.getCode());
									response.setMessage(httpsConResult.getTxMessage());
								}
							} else {
								
								response.setCode(ResponseCodes.PHONE_RECENTLY_RECHARGED.getCode());
								response.setMessage(ResponseCodes.PHONE_RECENTLY_RECHARGED.getMessage());
							}
						} else {
							// Log Sendtopup Request
							logSendTopupRequest(requestDto);

							httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(gson.toJson(requestDto1),
									connectionRequest);
							String str_result = httpsConResult.getResponseData();
							String status = httpsConResult.getTxMessage();

							// Check Response Code recieved in response is Success or not
							if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {

								// Convert response from Json to Object
								response = (SendTopupResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
										SendTopupResponseDto.class);
								LOGGER.info("SendTopupServiceImpl in sendTopup function " + response);
								// Log Response for sendtopup
								logResponse(response, requestDto, status);
							} else {

								logError(httpsConResult.getCode(), status, requestDto);
								response.setCode(httpsConResult.getCode());
								response.setMessage(httpsConResult.getTxMessage());
							}
						}
					} else {
					
						response.setCode(ResponseCodes.ER210.getCode());
						response.setMessage(ResponseCodes.ER210.getMessage());
					}
				} else {
					
					response.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					response.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in SendTopupServiceImpl" + de);
			
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in SendTopupServiceImpl" + e);
		
			response.setCode(ResponseCodes.E401.getCode());
			response.setMessage(ResponseCodes.E401.getMessage());
		}
		return response;
	}

	// Log Send topup Request
	private void logSendTopupRequest(SendTopupRequestDto requestDto) throws DaoException {

		ReloadlyTransactionLogModel reloadlyTransactionLogModel = new ReloadlyTransactionLogModel();

		reloadlyTransactionLogModel.setReciepientPhoneNumber(requestDto.getRecipientPhone().getNumber());
		reloadlyTransactionLogModel.setReciepientCountryCode(requestDto.getRecipientPhone().getCountryCode());
		reloadlyTransactionLogModel.setSenderCountryCode(requestDto.getSenderPhone().getCountryCode());
		reloadlyTransactionLogModel.setSenderPhoneNumber(requestDto.getSenderPhone().getNumber());
		reloadlyTransactionLogModel.setOperatorId(requestDto.getOperatorId());
		reloadlyTransactionLogModel.setCustomIdentifier(requestDto.getCustomIdentifier());
		reloadlyTransactionLogModel.setRequestedAmount(requestDto.getAmount());
		reloadlyTransactionLogModel.setDateLogged(new Date());
		reloadlyTransactionLogModel.setMfsTransId(requestDto.getMfsTransId());
		reloadlyTransactionLogModel.setStatus("Pending");
		transactionDao.save(reloadlyTransactionLogModel);
	}

	// Log SendTopup Response
	private void logResponse(SendTopupResponseDto response, SendTopupRequestDto requestDto, String status)
			throws DaoException {

		// get sendtopup details against the mfstransid
		ReloadlyTransactionLogModel reloadlyTransactionLogModel = transactionDao
				.getSendtopupByMfsTransId(requestDto.getMfsTransId());

		// Check if reloadlyTransactionLogModel null or not
		if (reloadlyTransactionLogModel != null) {
			reloadlyTransactionLogModel.setTransactionId(response.getTransactionId());
			reloadlyTransactionLogModel.setReciepientPhoneNumber(requestDto.getRecipientPhone().getNumber());
			reloadlyTransactionLogModel.setSenderPhoneNumber(requestDto.getSenderPhone().getNumber());
			reloadlyTransactionLogModel.setReciepientCountryCode(response.getCountryCode());
			reloadlyTransactionLogModel.setOperatorId(response.getOperatorId());
			reloadlyTransactionLogModel.setRequestedAmount(response.getRequestedAmount());
			reloadlyTransactionLogModel.setRequestedAmountCurrencyCode(response.getRequestedAmountCurrencyCode());
			reloadlyTransactionLogModel.setDeliveredAmount(response.getDeliveredAmount());
			reloadlyTransactionLogModel.setDeliveredAmountCurrencyCode(response.getDeliveredAmountCurrencyCode());
			reloadlyTransactionLogModel.setCustomIdentifier(response.getCustomIdentifier());
			reloadlyTransactionLogModel.setTransactionDate(response.getTransactionDate());
			reloadlyTransactionLogModel.setResponseDateLogged(new Date());
			reloadlyTransactionLogModel.setStatus(status);
			reloadlyTransactionLogModel.setOperatorTransactionId(response.getOperatorTransactionId());
			reloadlyTransactionLogModel.setDiscount(response.getDiscount());
			reloadlyTransactionLogModel.setDiscountCurrencyCode(response.getDiscountCurrencyCode());
			reloadlyTransactionLogModel.setOperatorName(response.getOperatorName());
			reloadlyTransactionLogModel.setPinDetail(response.getPinDetail());

			transactionDao.update(reloadlyTransactionLogModel);
		}
	}

	// Log Error
	private void logError(String code, String status, SendTopupRequestDto requestDto) throws DaoException {
		// TODO Auto-generated method stub
		ReloadlyTransactionErrorModel reloadlyTransactionErrorModel = new ReloadlyTransactionErrorModel();
		reloadlyTransactionErrorModel.setMfsTransId(requestDto.getMfsTransId());
		reloadlyTransactionErrorModel.setReciepientPhoneNumber(requestDto.getRecipientPhone().getNumber());
		reloadlyTransactionErrorModel.setReciepientCountryCode(requestDto.getRecipientPhone().getCountryCode());
		reloadlyTransactionErrorModel.setSenderPhoneNumber(requestDto.getSenderPhone().getNumber());
		reloadlyTransactionErrorModel.setSenderCountryCode(requestDto.getSenderPhone().getCountryCode());
		reloadlyTransactionErrorModel.setCustomIdentifier(requestDto.getCustomIdentifier());
		reloadlyTransactionErrorModel.setAmount(requestDto.getAmount());
		reloadlyTransactionErrorModel.setDateLogged(new Date());
		reloadlyTransactionErrorModel.setStatus(status);
		reloadlyTransactionErrorModel.setCode(code);
		transactionDao.save(reloadlyTransactionErrorModel);
	}
}
