package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.ContentResponseDto;

public interface GetOperatorByIdService {
	
	ContentResponseDto getOperatorById(String OperatorId);

}
