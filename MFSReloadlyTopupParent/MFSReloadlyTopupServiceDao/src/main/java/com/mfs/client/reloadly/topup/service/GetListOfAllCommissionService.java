package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.ListAllCommissionResponseDto;

public interface GetListOfAllCommissionService {

	public ListAllCommissionResponseDto listOfAllCommissionService(String page, String size);
}
