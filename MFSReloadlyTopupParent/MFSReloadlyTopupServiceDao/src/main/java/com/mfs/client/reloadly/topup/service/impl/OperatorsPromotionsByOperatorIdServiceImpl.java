package com.mfs.client.reloadly.topup.service.impl;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.dto.OperatorsPromotionsListByOperatorIdResponse;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByOperatorIdService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @authorSrishti OperatorsPromotionsByIdServiceImpl.java 12-02-2020
 * 
 *                purpose: to retrieve the topup operators promotions by
 *                operator id
 * 
 *                Methods: 1. getPromotionsByOperatorId
 * 
 */
@Service("OperatorsPromotionsByOperatorIdService")
public class OperatorsPromotionsByOperatorIdServiceImpl implements OperatorsPromotionsByOperatorIdService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(OperatorsPromotionsByOperatorIdServiceImpl.class);

	// method for getting topup operators promotions by operator id
	public OperatorsPromotionsListByOperatorIdResponse getPromotionsByOperatorId(String operatorId) {

		Map<String, String> header = new HashMap<String, String>();
		OperatorsPromotionsListByOperatorIdResponse responseDto = new OperatorsPromotionsListByOperatorIdResponse();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		Gson gson = new Gson();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		
		try {
			// get last generated token
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();

			// check null values for access token model
			if (accessTokenModel == null) {

				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());

			} else {

				Date lastDBDate = accessTokenModel.getDateLogged();

				// check for token expiration
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());

				if (flag) {

					// retrieve config data from DB
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();

					// check null value for config data
					if (configMap == null) {
						throw new Exception("No config details found");
					}

					// set url
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(
							configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.PROMOTIONS) + "/"
									+ configMap.get(CommonConstant.OPERATORS) + "/" + operatorId);

					// set headers
					header.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					header.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					header.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessTokenModel.getAccessToken());
					connectionRequest.setHeaders(header);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					// check for success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {

						Type collectionType = new TypeToken<List<ContentResponse>>() {
						}.getType();

						List<ContentResponse> contentList = gson.fromJson(str_result, collectionType);

						if (contentList.isEmpty()) {

							responseDto.setContent(contentList);
							responseDto.setCode(ResponseCodes.ORERATOR_NOT_FOUND.getCode());
							responseDto.setMessage(ResponseCodes.ORERATOR_NOT_FOUND.getMessage());
						}

						responseDto.setContent(contentList);

						LOGGER.info("OperatorsPromotionsByOperatorIdServiceImpl in getPromotionsByOperatorId function "
								+ responseDto);

					} else {
						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}

				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in OperatorsPromotionsByOperatorIdServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in OperatorsPromotionsByOperatorIdServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}

		return responseDto;
	}

}
