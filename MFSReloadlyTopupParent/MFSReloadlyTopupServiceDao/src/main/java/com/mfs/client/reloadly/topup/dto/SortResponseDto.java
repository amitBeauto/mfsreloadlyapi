package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author Priyanka Prakash Khade SortResponseDto.java 25-Jan-2020
 *
 *         purpose : this class is used for Sort response
 *         
 * Change_history : On 04-02-2020 While hitting live URL added fields -> Boolean empty
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SortResponseDto {

	private Boolean sorted;

	private Boolean unsorted;

	private Boolean empty;

	public Boolean getSorted() {
		return sorted;
	}

	public void setSorted(Boolean sorted) {
		this.sorted = sorted;
	}

	public Boolean getUnsorted() {
		return unsorted;
	}

	public void setUnsorted(Boolean unsorted) {
		this.unsorted = unsorted;
	}

	public Boolean getEmpty() {
		return empty;
	}

	public void setEmpty(Boolean empty) {
		this.empty = empty;
	}

	@Override
	public String toString() {
		return "SortResponseDto [sorted=" + sorted + ", unsorted=" + unsorted + ", empty=" + empty + "]";
	}

}
