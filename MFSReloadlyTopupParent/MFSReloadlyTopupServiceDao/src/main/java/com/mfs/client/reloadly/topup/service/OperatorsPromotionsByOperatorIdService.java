package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.OperatorsPromotionsListByOperatorIdResponse;

public interface OperatorsPromotionsByOperatorIdService {

	public OperatorsPromotionsListByOperatorIdResponse getPromotionsByOperatorId(String operatorId);

}
