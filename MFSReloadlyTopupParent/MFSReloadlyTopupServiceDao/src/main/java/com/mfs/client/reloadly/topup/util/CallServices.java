package com.mfs.client.reloadly.topup.util;

import java.util.HashMap;
import java.util.Map;

public class CallServices {

	public static String getResponseFromService(String url, String subscriptionKey) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;

		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			// headerMap.put(CommonConstant.AUTHORIZATION, signature);
			//headerMap.put("Ocp-Apim-Subscription-Key", subscriptionKey);

			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");

			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, subscriptionKey);
			str_result = httpsConResult.getResponseData();

		} catch (Exception e) {
			throw new Exception(e);
		}
		return str_result;
	}
}
