package com.mfs.client.reloadly.topup.util;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.mfs.client.reloadly.topup.dao.TransactionDao;

public class ExpiredToken {

	@Autowired
	static TransactionDao transactionDao;

	public static boolean isTokenExpire(Date lastDBDate, Date CurrentDate) {

		long diffTime = 0;

		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();

		start.setTime(lastDBDate);
		end.setTime(CurrentDate);

		Date startDate = start.getTime();
		Date endDate = end.getTime();

		long startTime = startDate.getTime();
		long endTime = endDate.getTime();

		diffTime = endTime - startTime;

		long minutes = (diffTime / 1000) / 60;
		long seconds = diffTime / 1000;
		long daydiff = (diffTime / (60 * 60 * 24 * 1000));

		int day = CommonConstant.ONE_DAY;
		int oneDayMinuts = CommonConstant.MINUETS;
		int oneDaySeconds = CommonConstant.SECONDS;

		if (daydiff <= day && minutes <= oneDayMinuts && seconds <= oneDaySeconds) {
			return true;
		}

		return false;

	}

}