package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.AccountCommissionByOperatorIdResponseDto;

public interface AccountCommissionServiceByOperatorIdService {

	AccountCommissionByOperatorIdResponseDto accountCommissionByOperatorId(String opretorId);

}
