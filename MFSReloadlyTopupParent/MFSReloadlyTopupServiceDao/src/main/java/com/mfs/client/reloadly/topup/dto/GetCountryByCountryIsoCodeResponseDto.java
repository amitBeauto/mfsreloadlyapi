package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class GetCountryByCountryIsoCodeResponseDto {
	
	private String isoName;
	private String name;
	private String currencyCode;
	private String currencyName;
	private String currencySymbol;
	private String flag;
	private List<String>callingCodes;
	
	private String code;
	private String message;
	
	public String getIsoName() {
		return isoName;
	}
	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public List<String> getCallingCodes() {
		return callingCodes;
	}
	public void setCallingCodes(List<String> callingCodes) {
		this.callingCodes = callingCodes;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "GetCountryByCountryIsoCodeResponseDto [isoName=" + isoName + ", name=" + name + ", currencyCode="
				+ currencyCode + ", currencyName=" + currencyName + ", currencySymbol=" + currencySymbol + ", flag="
				+ flag + ", callingCodes=" + callingCodes + ", code=" + code + ", message=" + message + "]";
	}

}
