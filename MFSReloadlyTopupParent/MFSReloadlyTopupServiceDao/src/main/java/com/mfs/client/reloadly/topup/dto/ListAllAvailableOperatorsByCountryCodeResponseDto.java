package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class ListAllAvailableOperatorsByCountryCodeResponseDto {

	List<ListAvailableOperatorsByCountryCodeDto> listAvailableOperatorByCountryCode;

	private String code;
	private String message;
	
	
	public List<ListAvailableOperatorsByCountryCodeDto> getListAvailableOperatorByCountryCode() {
		return listAvailableOperatorByCountryCode;
	}
	public void setListAvailableOperatorByCountryCode(
			List<ListAvailableOperatorsByCountryCodeDto> listAvailableOperatorByCountryCode) {
		this.listAvailableOperatorByCountryCode = listAvailableOperatorByCountryCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "ListAllAvailableOperatorsByCountryCodeResponseDto [listAvailableOperatorByCountryCode="
				+ listAvailableOperatorByCountryCode + ", code=" + code + ", message=" + message + "]";
	}

}
