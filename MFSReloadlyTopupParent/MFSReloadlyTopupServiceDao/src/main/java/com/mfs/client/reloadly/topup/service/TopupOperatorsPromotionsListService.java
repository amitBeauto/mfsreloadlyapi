package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.TopUpOperatorsPromotionsResponseDto;

public interface TopupOperatorsPromotionsListService {
	
	TopUpOperatorsPromotionsResponseDto getListOfTopupOperatorsPromotions();

}
