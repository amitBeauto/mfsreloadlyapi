package com.mfs.client.reloadly.topup.service;

/**
 * @ author Mohsin Shaikh
 *  ListSupportedCountryService.java 
 *  30-Jan-2020
 * 
 * purpose : this interface is used to get ListSupportedCountries
 * 
 * Methods 
 * 1.getListSupportedCountry
 * 
 * Change_history
 */
import com.mfs.client.reloadly.topup.dto.ListSupportedCountriesResponseDto;

public interface ListSupportedCountryService {

	ListSupportedCountriesResponseDto getListSupportedCountry();

}
