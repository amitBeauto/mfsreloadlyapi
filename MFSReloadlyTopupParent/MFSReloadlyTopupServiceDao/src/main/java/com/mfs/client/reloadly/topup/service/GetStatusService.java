package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;

import com.mfs.client.reloadly.topup.dto.GetStatusResponseDto;

public interface GetStatusService {
	
	GetStatusResponseDto getStatus(GetStatusRequestDto request);

}
