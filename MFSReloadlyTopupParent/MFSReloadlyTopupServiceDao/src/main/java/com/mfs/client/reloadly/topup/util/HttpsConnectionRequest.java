package com.mfs.client.reloadly.topup.util;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class HttpsConnectionRequest {

	private String serviceUrl;

	private String httpmethodName;

	private int port;

	private boolean isHttps=true;
	
	

	private Map<String, String> headers;

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public String getHttpmethodName() {
		return httpmethodName;
	}

	public void setHttpmethodName(String httpmethodName) {
		this.httpmethodName = httpmethodName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	

	public boolean isHttps() {
		return isHttps;
	}

	public void setHttps(boolean isHttps) {
		this.isHttps = isHttps;
	}

	@Override
	public String toString() {
		return "HttpsConnectionRequest [serviceUrl=" + serviceUrl + ", httpmethodName=" + httpmethodName + ", port="
				+ port + ", isHttps=" + isHttps + ", headers=" + headers + "]";
	}


}
