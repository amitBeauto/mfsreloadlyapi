package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_operators")
public class ReloadlyContentModel {

	@Id
	@GeneratedValue
	@Column(name = "content_id")
	private int contentId;

	@Column(name = "operator_id")
	private String operatorId;

	@Column(name = "name")
	private String name;

	@Column(name = "denomination_type")
	private String denominationType;

	@Column(name = "sender_currency_code")
	private String senderCurrencyCode;

	@Column(name = "sender_currency_symbol")
	private String senderCurrencySymbol;

	@Column(name = "destination_currency_code")
	private String destinationCurrencyCode;

	@Column(name = "destination_currency_symbol")
	private String destinationCurrencySymbol;

	@Column(name = "commission")
	private String commission;

	@Column(name = "international_discount")
	private String internationalDiscount;

	@Column(name = "local_discount")
	private String localDiscount;

	@Column(name = "most_popular_amount")
	private String mostPopularAmount;

	@Column(name = "iso_name")
	private String isoName;

	@Column(name = "suggested_amounts", length = 500)
	private String suggestedAmounts;

	@Column(name = "suggested_amounts_map", length = 500)
	private String suggestedAmountsMap;

	@Column(name = "min_amount")
	private String minAmount;

	@Column(name = "max_amount")
	private String maxAmount;

	@Column(name = "fixed_amounts", length = 500)
	private String fixedAmounts;

	@Column(name = "logo_urls", columnDefinition = "text", length = 65535)
	private String logoUrls;

	@Column(name = "date_logged")
	private Date dateLogged;

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public int getContentId() {
		return contentId;
	}

	public void setContentId(int contentId) {
		this.contentId = contentId;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDenominationType() {
		return denominationType;
	}

	public void setDenominationType(String denominationType) {
		this.denominationType = denominationType;
	}

	public String getSenderCurrencyCode() {
		return senderCurrencyCode;
	}

	public void setSenderCurrencyCode(String senderCurrencyCode) {
		this.senderCurrencyCode = senderCurrencyCode;
	}

	public String getSenderCurrencySymbol() {
		return senderCurrencySymbol;
	}

	public void setSenderCurrencySymbol(String senderCurrencySymbol) {
		this.senderCurrencySymbol = senderCurrencySymbol;
	}

	public String getDestinationCurrencyCode() {
		return destinationCurrencyCode;
	}

	public void setDestinationCurrencyCode(String destinationCurrencyCode) {
		this.destinationCurrencyCode = destinationCurrencyCode;
	}

	public String getDestinationCurrencySymbol() {
		return destinationCurrencySymbol;
	}

	public void setDestinationCurrencySymbol(String destinationCurrencySymbol) {
		this.destinationCurrencySymbol = destinationCurrencySymbol;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getInternationalDiscount() {
		return internationalDiscount;
	}

	public void setInternationalDiscount(String internationalDiscount) {
		this.internationalDiscount = internationalDiscount;
	}

	public String getLocalDiscount() {
		return localDiscount;
	}

	public void setLocalDiscount(String localDiscount) {
		this.localDiscount = localDiscount;
	}

	public String getMostPopularAmount() {
		return mostPopularAmount;
	}

	public void setMostPopularAmount(String mostPopularAmount) {
		this.mostPopularAmount = mostPopularAmount;
	}

	public String getIsoName() {
		return isoName;
	}

	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}

	public String getSuggestedAmounts() {
		return suggestedAmounts;
	}

	public void setSuggestedAmounts(String suggestedAmounts) {
		this.suggestedAmounts = suggestedAmounts;
	}

	public String getSuggestedAmountsMap() {
		return suggestedAmountsMap;
	}

	public void setSuggestedAmountsMap(String suggestedAmountsMap) {
		this.suggestedAmountsMap = suggestedAmountsMap;
	}

	public String getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}

	public String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getFixedAmounts() {
		return fixedAmounts;
	}

	public void setFixedAmounts(String fixedAmounts) {
		this.fixedAmounts = fixedAmounts;
	}

	public String getLogoUrls() {
		return logoUrls;
	}

	public void setLogoUrls(String logoUrls) {
		this.logoUrls = logoUrls;
	}

	@Override
	public String toString() {
		return "ReloadlyContentModel [contentId=" + contentId + ", operatorId=" + operatorId + ", name=" + name
				+ ", denominationType=" + denominationType + ", senderCurrencyCode=" + senderCurrencyCode
				+ ", senderCurrencySymbol=" + senderCurrencySymbol + ", destinationCurrencyCode="
				+ destinationCurrencyCode + ", destinationCurrencySymbol=" + destinationCurrencySymbol + ", commission="
				+ commission + ", internationalDiscount=" + internationalDiscount + ", localDiscount=" + localDiscount
				+ ", mostPopularAmount=" + mostPopularAmount + ", isoName=" + isoName + ", suggestedAmounts="
				+ suggestedAmounts + ", suggestedAmountsMap=" + suggestedAmountsMap + ", minAmount=" + minAmount
				+ ", maxAmount=" + maxAmount + ", fixedAmounts=" + fixedAmounts + ", logoUrls=" + logoUrls
				+ ", dateLogged=" + dateLogged + "]";
	}
}