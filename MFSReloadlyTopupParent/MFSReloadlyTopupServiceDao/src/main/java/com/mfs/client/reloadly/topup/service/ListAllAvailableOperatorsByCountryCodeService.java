package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.ListAllAvailableOperatorsByCountryCodeResponseDto;

public interface ListAllAvailableOperatorsByCountryCodeService {
	
	ListAllAvailableOperatorsByCountryCodeResponseDto getAllAvailableOpreatorsByCountryCode(String countryIsoCode);

}
