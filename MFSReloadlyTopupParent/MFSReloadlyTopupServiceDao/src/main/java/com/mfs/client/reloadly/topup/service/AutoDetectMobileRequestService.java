package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.AutoDetectMobileRequestDto;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileResponseDto;

/*
 * @author Pallavi Gohite
 * AutoDetectMobileRequestService.java
 * 21-01-2020
 * 
 * purpose: this class is used for Interface Service Auto-Detect Mobile
 * 
 */

public interface AutoDetectMobileRequestService {

	public AutoDetectMobileResponseDto autoDetectMobile(AutoDetectMobileRequestDto request);

}
