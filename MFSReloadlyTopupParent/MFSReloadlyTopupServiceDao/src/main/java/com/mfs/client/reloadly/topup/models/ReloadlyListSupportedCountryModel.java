package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_list_supported_country")
public class ReloadlyListSupportedCountryModel {

	@Id
	@GeneratedValue
	@Column(name = "list_supported_country_id")
	private int listSupportedCountryId;

	@Column(name = "iso_name")
	private String isoName;

	@Column(name = "country_name")
	private String name;

	@Column(name = "currency_code")
	private String currencyCode;

	@Column(name = "currency_name")
	private String currencyName;

	@Column(name = "flag")
	private String flag;

	@Column(name = "country_calling_codes")
	private String callingCodes;

	@Column(name = "country_currency_symbol")
	private String currencySymbol;
	
	@Column(name = "date_logged")
	private Date dateLogged;


	public int getListSupportedCountryId() {
		return listSupportedCountryId;
	}

	public void setListSupportedCountryId(int listSupportedCountryId) {
		this.listSupportedCountryId = listSupportedCountryId;
	}

	public String getIsoName() {
		return isoName;
	}

	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCallingCodes() {
		return callingCodes;
	}

	public void setCallingCodes(String callingCodes) {
		this.callingCodes = callingCodes;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "ReloadlyListSupportedCountryModel [listSupportedCountryId=" + listSupportedCountryId + ", isoName="
				+ isoName + ", name=" + name + ", currencyCode=" + currencyCode + ", currencyName=" + currencyName
				+ ", flag=" + flag + ", callingCodes=" + callingCodes + ", currencySymbol=" + currencySymbol
				+ ", dateLogged=" + dateLogged + "]";
	}
	
	

}
