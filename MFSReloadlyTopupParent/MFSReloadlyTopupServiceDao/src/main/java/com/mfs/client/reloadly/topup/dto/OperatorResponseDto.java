package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.mfs.client.reloadly.topup.models.ReloadlyOperatorsPromotionsModel;

public class OperatorResponseDto {
	private String operatorId;
	private String name;
	private String countryCode;
	private String status;
	private String bundle;
	private String internationalDiscount;
	private String localDiscount;
	private String commission;
	private List<ContentResponse> promotions;

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getInternationalDiscount() {
		return internationalDiscount;
	}

	public void setInternationalDiscount(String internationalDiscount) {
		this.internationalDiscount = internationalDiscount;
	}

	public String getLocalDiscount() {
		return localDiscount;
	}

	public void setLocalDiscount(String localDiscount) {
		this.localDiscount = localDiscount;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public List<ContentResponse> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<ContentResponse> promotions) {
		this.promotions = promotions;
	}

	@Override
	public String toString() {
		return "OperatorResponseDto [operatorId=" + operatorId + ", name=" + name + ", countryCode=" + countryCode
				+ ", status=" + status + ", bundle=" + bundle + ", internationalDiscount=" + internationalDiscount
				+ ", localDiscount=" + localDiscount + ", commission=" + commission + ", promotions=" + promotions
				+ "]";
	}

}
