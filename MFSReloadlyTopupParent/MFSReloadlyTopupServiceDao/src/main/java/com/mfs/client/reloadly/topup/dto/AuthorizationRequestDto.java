package com.mfs.client.reloadly.topup.dto;

/**
 * @ author Sudarshan B Ghorpade
 *  AuthorizationRequestDto.java 
 *  24-Jan-2020
 * 
 * purpose : this class is used for authorization request
 * 
 * Change_history
 */
public class AuthorizationRequestDto {

	private String client_id;
	private String client_secret;
	private String grant_type;
	private String audience;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	public String getGrant_type() {
		return grant_type;
	}

	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}

	public String getAudience() {
		return audience;
	}

	public void setAudience(String audience) {
		this.audience = audience;
	}

	@Override
	public String toString() {
		return "AuthenticationRequestDto [client_id=" + client_id + ", client_secret=" + client_secret + ", grant_type="
				+ grant_type + ", audience=" + audience + "]";
	}

}
