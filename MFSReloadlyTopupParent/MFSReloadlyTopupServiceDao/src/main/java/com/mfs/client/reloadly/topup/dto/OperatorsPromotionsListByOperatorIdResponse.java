package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class OperatorsPromotionsListByOperatorIdResponse {

	private List<ContentResponse> content;

	private String code;

	private String message;

	public List<ContentResponse> getContent() {
		return content;
	}

	public void setContent(List<ContentResponse> content) {
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ListOfOperatorsPromotionsByOperatorId [content=" + content + ", code=" + code + ", message=" + message
				+ "]";
	}

}
