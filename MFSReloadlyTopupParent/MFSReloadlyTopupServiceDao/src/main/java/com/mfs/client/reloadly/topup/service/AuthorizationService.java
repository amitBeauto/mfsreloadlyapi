package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.AuthorizationResponseDto;

/**
 * @ author Sudarshan B Ghorpade
 *  AuthorizationService.java 
 *  24-Jan-2020
 * 
 * purpose : this interface is used for generate the access token
 * 
 * Methods 
 * 1.authorizationService
 * 
 * Change_history
 */
public interface AuthorizationService {
	
	public AuthorizationResponseDto authorizationService();

}
