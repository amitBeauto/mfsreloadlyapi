package com.mfs.client.reloadly.topup.dao;

import java.util.List;

import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyCommissionsModel;
import com.mfs.client.reloadly.topup.models.ReloadlyContentModel;
import com.mfs.client.reloadly.topup.models.ReloadlyListSupportedCountryModel;
import com.mfs.client.reloadly.topup.models.ReloadlyOperatorsPromotionsModel;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionLogModel;

public interface TransactionDao extends BaseDao {

	public ReloadlyAuthorizationModel getLastRecordByAccessTokenId();

	public ReloadlyContentModel getByOperatorId(String operatorId) throws DaoException;

	ReloadlyTransactionLogModel getSendtopupByMfsTransId(String mfsTransId) throws DaoException;

	ReloadlyListSupportedCountryModel getListCountryByCountryName(String string) throws DaoException;

	List<ReloadlyTransactionLogModel> getLastRecordtransactionLogId(String string, String string2) throws DaoException;

	public ReloadlyOperatorsPromotionsModel getByPromotionId(String promotionId) throws DaoException;

	public ReloadlyCommissionsModel getCommissionByOperatorId(String operatorid)throws DaoException;
	
	ReloadlyTransactionLogModel getTransLogDataByMfsTransId(String mfsTransId)throws DaoException;

}
