package com.mfs.client.reloadly.topup.service;

import java.util.Map;

import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SendTopupResponseDto;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;

public interface TopupService {

	SendTopupResponseDto topup(SendTopupRequestDto requestDto, ReloadlyAuthorizationModel reloadlyAuthorizationModel, Map<String, String> systemConfigDetails);

	
}
