package com.mfs.client.reloadly.topup.dto;

public class ListAllOperatorContent {
	private String internationalPercentage;
	private String localPercentage;
	private String updatedAt;
	private OperatorResponseDto operator;
	private String percentage;
	
	public String getInternationalPercentage() {
		return internationalPercentage;
	}
	public void setInternationalPercentage(String internationalPercentage) {
		this.internationalPercentage = internationalPercentage;
	}
	public String getLocalPercentage() {
		return localPercentage;
	}
	public void setLocalPercentage(String localPercentage) {
		this.localPercentage = localPercentage;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public OperatorResponseDto getOperator() {
		return operator;
	}
	public void setOperator(OperatorResponseDto operator) {
		this.operator = operator;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	@Override
	public String toString() {
		return "ListAllOperatorContent [internationalPercentage=" + internationalPercentage + ", localPercentage="
				+ localPercentage + ", updatedAt=" + updatedAt + ", operator=" + operator + ", percentage=" + percentage
				+ "]";
	}
	

}
