package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SendTopupResponseDto;

/**
 * @ author Mohsin Shaikh
 *  SendTopupService.java 
 *  30-Jan-2020
 * 
 * purpose : this interface is used to sendtopup
 * 
 * Methods 
 * 1.sendTopup
 * 
 * Change_history
 */
public interface SendTopupService {

	SendTopupResponseDto sendTopup(SendTopupRequestDto requestDto);

}
