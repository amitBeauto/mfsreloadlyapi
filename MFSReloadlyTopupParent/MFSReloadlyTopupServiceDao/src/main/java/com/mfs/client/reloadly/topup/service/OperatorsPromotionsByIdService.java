package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.ContentResponse;

public interface OperatorsPromotionsByIdService {

	public ContentResponse getPromotionsById(String promotionId);

}
