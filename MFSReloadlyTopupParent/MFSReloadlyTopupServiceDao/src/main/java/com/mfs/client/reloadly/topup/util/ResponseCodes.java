package com.mfs.client.reloadly.topup.util;

public enum ResponseCodes {

	E401("E401", "MFS System Error "), 
	S200("200", "Success"),
	S201("201", "Created"),
	S202("202", "Accepted"), 
	S204("204", "No content"),
	
	VALIDATION_ERROR("ER206", "Validation Error"), 
	
	ER204("ER204", "Exception while save data"),
	ER205("ER205", "Exception while update"),
    ER206("ER206", "Exception while getting last record by accessTokenId"),
	ER207("ER207", "Exception while processing your request, Please try again later."),
	ER208("ER208","Exception while getting Operator Id"),
	ER400("ER400", "Bad Request"), 
	ER401("ER401", "Unauthorized"), 
	ER403("ER403", "Not_Authorized"),
	ER404("ER404", "Resource not found"),
	ER405("ER405", "Method not supported"),
	ER406("ER406", "Media type not acceptable"),
	ER415("ER415", "Unsupported media type"),
	ER422("ER422", "Unproccessable entity"), 
	ER429("ER429", "Rate limit reached"),
	ER500("ER500", "Internal server error"), 
	ER503("ER503", "Service Unavailable"),
	ER209("ER209","Request Details Cannot be Null."),
	ER210("ER210","Duplicate record found i.e is MfsTransId"),
	ER211("ER211","Exception while getting mfsTransId"),
	ER212("ER212","Exception while getting country name"),
	ER213("ER213","Exception while getting reciepientPhone number and senderPhone Number"),

	ORERATOR_NOT_FOUND("OPERATOR_NOT_FOUND", "No such promotions found against the operator"),
	INVALID_REQUEST("INVALID_REQUEST", "Request is not well-formed, syntactically incorrect, or violates schema."),
	AUTHENTICATION_FAILURE("AUTHENTICATION_FAILURE",
			"Authentication failed due to invalid authentication credentials."),
	NOT_AUTHORIZED("NOT_AUTHORIZED", "Authorization failed due to insufficient permissions."),
	RESOURCE_NOT_FOUND("RESOURCE_NOT_FOUND", "The specified resource does not exist."),
	METHOD_NOT_SUPPORTED("METHOD_NOT_SUPPORTED", "he server does not implement the requested HTTP method."),
	MEDIA_TYPE_NOT_ACCEPTABLE("MEDIA_TYPE_NOT_ACCEPTABLE",
			"The server does not implement the media type that would be acceptable to the client."),
	UNSUPPORTED_MEDIA_TYPE("UNSUPPORTED_MEDIA_TYPE", "The server does not support the request payload’s media type."),
	UNPROCCESSABLE_ENTITY("UNPROCCESSABLE_ENTITY",
			"The API cannot complete the requested action, or the request action is semantically incorrect or fails business validation."),
	RATE_LIMIT_REACHED("RATE_LIMIT_REACHED", "Too many requests. Blocked due to rate limiting."),
	INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", "An internal server error has occurred."),
	SERVICE_UNAVAILABLE("SERVICE_UNAVAILABLE", "Service Unavailable."),
	COUNTRY_NOT_SUPPORTED("COUNTRY_NOT_SUPPORTED","request country code is currently not supported"),
	INVALID_PHONE_NUMBER("INVALID_PHONE_NUMBER","The specified phone number is not valid"),

	COULD_NOT_AUTO_DETECT_OPERATOR("COULD_NOT_AUTO_DETECT_OPERATOR","Could not auto detect operator"),

	PHONE_RECENTLY_RECHARGED("PHONE_RECENTLY_RECHARGED","Phone recently recharged , Try again after 2 minutes. "),
	TOKEN_EXPIRED("TOKEN_EXPIRED","Token Expired"),
    ERROR_CODE("ERROR_CODE","Account verification failed, invalid or expired token"), 
    REQUIRED_PHONE_NUMBER("REQUIRED_PHONE_NUMBER","Please provide phone number"),
    REQUIRED_COUNTRY_CODE("REQUIRED_COUNTRY_CODE","Please provide country code"),
    UNAUTHORIZED_TOKEN("UNAUTHORIZED_TOKEN", "Null Access Token"),
    MFS_TRANS_ID_NOT_FOUND("MFS_TRANS_ID_NOT_FOUND","No transaction found against mfsTransId");	

	private String code;
	private String message;

	private ResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
