package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_commission")
public class ReloadlyCommissionsModel {

	@Id
	@GeneratedValue
	@Column(name = "commission_id")
	private int commissionId;

	@Column(name = "content_international_percentage")
	private String contentInternationalPercentage;

	@Column(name = "content_local_percentage")
	private String contentLocalPercentage;

	@Column(name = "content_updated_at")
	private String contentUpdatedAt;

	@Column(name = "content_percentage")
	private String contentPercentage;

	@Column(name = "operator_operator_id")
	private String operatorOperatorId;

	@Column(name = "operator_status")
	private String operatorStatus;

	
	@Column(name = "operator_bundle")
	private String operatorBundle;

	@Column(name = "operator_commission")
	private String operatorCommission;

	
	@Column(name = "operator_promotions_id")
	private String operatorPromotionsId;
	
	@Column(name = "date_logged")
	private Date dateLogged;

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public int getCommissionId() {
		return commissionId;
	}

	public void setCommissionId(int commissionId) {
		this.commissionId = commissionId;
	}

	public String getContentInternationalPercentage() {
		return contentInternationalPercentage;
	}

	public void setContentInternationalPercentage(String contentInternationalPercentage) {
		this.contentInternationalPercentage = contentInternationalPercentage;
	}

	public String getContentLocalPercentage() {
		return contentLocalPercentage;
	}

	public void setContentLocalPercentage(String contentLocalPercentage) {
		this.contentLocalPercentage = contentLocalPercentage;
	}

	public String getContentUpdatedAt() {
		return contentUpdatedAt;
	}

	public void setContentUpdatedAt(String contentUpdatedAt) {
		this.contentUpdatedAt = contentUpdatedAt;
	}

	public String getContentPercentage() {
		return contentPercentage;
	}

	public void setContentPercentage(String contentPercentage) {
		this.contentPercentage = contentPercentage;
	}

	public String getOperatorOperatorId() {
		return operatorOperatorId;
	}

	public void setOperatorOperatorId(String operatorOperatorId) {
		this.operatorOperatorId = operatorOperatorId;
	}

	public String getOperatorStatus() {
		return operatorStatus;
	}

	public void setOperatorStatus(String operatorStatus) {
		this.operatorStatus = operatorStatus;
	}

	public String getOperatorBundle() {
		return operatorBundle;
	}

	public void setOperatorBundle(String operatorBundle) {
		this.operatorBundle = operatorBundle;
	}
	public String getOperatorCommission() {
		return operatorCommission;
	}

	public void setOperatorCommission(String operatorCommission) {
		this.operatorCommission = operatorCommission;
	}

	public String getOperatorPromotionsId() {
		return operatorPromotionsId;
	}

	public void setOperatorPromotionsId(String operatorPromotionsId) {
		this.operatorPromotionsId = operatorPromotionsId;
	}

	@Override
	public String toString() {
		return "ReloadlyCommissionsModel [commissionId=" + commissionId + ", contentInternationalPercentage="
				+ contentInternationalPercentage + ", contentLocalPercentage=" + contentLocalPercentage
				+ ", contentUpdatedAt=" + contentUpdatedAt + ", contentPercentage=" + contentPercentage
				+ ", operatorOperatorId=" + operatorOperatorId + ", operatorStatus=" + operatorStatus
				+ ", operatorBundle=" + operatorBundle + ", operatorCommission=" + operatorCommission
				+ ", operatorPromotionsId=" + operatorPromotionsId + ", dateLogged=" + dateLogged + "]";
	}

}
