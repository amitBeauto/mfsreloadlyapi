package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class SendTopupResponseDto {

	private int transactionId;
	private String recipientPhone;
	private String senderPhone;
	private String countryCode;
	private int operatorId;
	private double requestedAmount;
	private String requestedAmountCurrencyCode;
	private double deliveredAmount;
	private String deliveredAmountCurrencyCode;
	private String customIdentifier;
	private String transactionDate;
	private String code;
	private String message;

	private String operatorTransactionId;
	private double discount;
	private String pinDetail;
	private String operatorName;
	private String discountCurrencyCode;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getRecipientPhone() {
		return recipientPhone;
	}

	public void setRecipientPhone(String recipientPhone) {
		this.recipientPhone = recipientPhone;
	}

	public String getSenderPhone() {
		return senderPhone;
	}

	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public String getRequestedAmountCurrencyCode() {
		return requestedAmountCurrencyCode;
	}

	public void setRequestedAmountCurrencyCode(String requestedAmountCurrencyCode) {
		this.requestedAmountCurrencyCode = requestedAmountCurrencyCode;
	}

	public double getDeliveredAmount() {
		return deliveredAmount;
	}

	public void setDeliveredAmount(double deliveredAmount) {
		this.deliveredAmount = deliveredAmount;
	}

	public String getDeliveredAmountCurrencyCode() {
		return deliveredAmountCurrencyCode;
	}

	public void setDeliveredAmountCurrencyCode(String deliveredAmountCurrencyCode) {
		this.deliveredAmountCurrencyCode = deliveredAmountCurrencyCode;
	}

	public String getCustomIdentifier() {
		return customIdentifier;
	}

	public void setCustomIdentifier(String customIdentifier) {
		this.customIdentifier = customIdentifier;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getOperatorTransactionId() {
		return operatorTransactionId;
	}

	public void setOperatorTransactionId(String operatorTransactionId) {
		this.operatorTransactionId = operatorTransactionId;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getPinDetail() {
		return pinDetail;
	}

	public void setPinDetail(String pinDetail) {
		this.pinDetail = pinDetail;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getDiscountCurrencyCode() {
		return discountCurrencyCode;
	}

	public void setDiscountCurrencyCode(String discountCurrencyCode) {
		this.discountCurrencyCode = discountCurrencyCode;
	}

	@Override
	public String toString() {
		return "SendTopupResponseDto [transactionId=" + transactionId + ", recipientPhone=" + recipientPhone
				+ ", senderPhone=" + senderPhone + ", countryCode=" + countryCode + ", operatorId=" + operatorId
				+ ", requestedAmount=" + requestedAmount + ", requestedAmountCurrencyCode="
				+ requestedAmountCurrencyCode + ", deliveredAmount=" + deliveredAmount
				+ ", deliveredAmountCurrencyCode=" + deliveredAmountCurrencyCode + ", customIdentifier="
				+ customIdentifier + ", transactionDate=" + transactionDate + ", code=" + code + ", message=" + message
				+ ", operatorTransactionId=" + operatorTransactionId + ", discount=" + discount + ", pinDetail="
				+ pinDetail + ", operatorName=" + operatorName + ", discountCurrencyCode=" + discountCurrencyCode + "]";
	}

}