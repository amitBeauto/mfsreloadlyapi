package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @authorSrishtiRDeo GetListAllOperatorsResponseDto.java 11-February-2020
 *
 *                    purpose : this class is used for content response
 */

@JsonInclude(Include.NON_NULL)

public class ContentResponse {

	private String promotionId;

	private String operatorId;

	private String title;

	private String title2;

	private String description;

	private String startDate;

	private String endDate;

	private String denominations;

	private String localDenominations;

	private String code;

	private String message;

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDenominations() {
		return denominations;
	}

	public void setDenominations(String denominations) {
		this.denominations = denominations;
	}

	public String getLocalDenominations() {
		return localDenominations;
	}

	public void setLocalDenominations(String localDenominations) {
		this.localDenominations = localDenominations;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ContentResponse [promotionId=" + promotionId + ", operatorId=" + operatorId + ", title=" + title
				+ ", title2=" + title2 + ", description=" + description + ", startDate=" + startDate + ", endDate="
				+ endDate + ", denominations=" + denominations + ", localDenominations=" + localDenominations
				+ ", code=" + code + ", message=" + message + "]";
	}

}
