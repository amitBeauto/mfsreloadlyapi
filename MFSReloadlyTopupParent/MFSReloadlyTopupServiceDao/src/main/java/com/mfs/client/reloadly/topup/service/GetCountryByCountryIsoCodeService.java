package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.GetCountryByCountryIsoCodeResponseDto;

public interface GetCountryByCountryIsoCodeService {
	
	GetCountryByCountryIsoCodeResponseDto getCountryByCountryIsoCode(String countryIsoCode);

}
