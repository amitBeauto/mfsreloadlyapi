package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ListSupportedCountriesResponseDto {

	private List<ListAllSupportedCountriesResponseDto> listAllSupportedCountriesResponseDto;
	private String code;
	private String message;

	public List<ListAllSupportedCountriesResponseDto> getListAllSupportedCountriesResponseDto() {
		return listAllSupportedCountriesResponseDto;
	}

	public void setListAllSupportedCountriesResponseDto(
			List<ListAllSupportedCountriesResponseDto> listAllSupportedCountriesResponseDto) {
		this.listAllSupportedCountriesResponseDto = listAllSupportedCountriesResponseDto;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ListSupportedCountriesResponseDto [listAllSupportedCountriesResponseDto="
				+ listAllSupportedCountriesResponseDto + ", code=" + code + ", message=" + message + "]";
	}

}
