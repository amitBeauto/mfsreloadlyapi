package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_operators_promotions")
public class ReloadlyOperatorsPromotionsModel {

	@Id
	@GeneratedValue
	@Column(name = "operators_promotion_id")
	private int OperatorsPromotionsId;

	@Column(name = "promotion_id")
	private String promotionId;

	@Column(name = "operator_id")
	private String operatorId;

	@Column(name = "title")
	private String title;

	@Column(name = "title2")
	private String title2;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "start_date")
	private String startDate;

	@Column(name = "end_date")
	private String endDate;

	@Column(name = "denominations")
	private String denominations;

	@Column(name = "local_denominations")
	private String localDenominations;

	@Column(name = "date_logged")
	private Date dateLogged;
	
	

	public int getOperatorsPromotionsId() {
		return OperatorsPromotionsId;
	}

	public void setOperatorsPromotionsId(int operatorsPromotionsId) {
		OperatorsPromotionsId = operatorsPromotionsId;
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDenominations() {
		return denominations;
	}

	public void setDenominations(String denominations) {
		this.denominations = denominations;
	}

	public String getLocalDenominations() {
		return localDenominations;
	}

	public void setLocalDenominations(String localDenominations) {
		this.localDenominations = localDenominations;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	
	
	

	@Override
	public String toString() {
		return "ReloadlyOperatorsPromotionsModel [OperatorsPromotionsId=" + OperatorsPromotionsId + ", promotionId="
				+ promotionId + ", operatorId=" + operatorId + ", title=" + title + ", title2=" + title2
				+ ", description=" + description + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", denominations=" + denominations + ", localDenominations=" + localDenominations + ", dateLogged="
				+ dateLogged + "]";
	}

}
