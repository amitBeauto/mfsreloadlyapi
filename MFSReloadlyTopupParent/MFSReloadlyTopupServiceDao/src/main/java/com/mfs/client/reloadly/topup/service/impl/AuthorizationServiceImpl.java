package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.AuthorizationRequestDto;
import com.mfs.client.reloadly.topup.dto.AuthorizationResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.AuthorizationService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * @ author Sudarshan B Ghorpade AuthorizationServiceImpl.java 24-Jan-2020
 * 
 * purpose : this class is used for generate the access token
 * 
 * Methods 1.authorizationService
 * 
 * Change_history
 * 
 */
@Service("AuthorizationService")
public class AuthorizationServiceImpl implements AuthorizationService {

	private static final Logger LOGGER = Logger.getLogger(AuthorizationServiceImpl.class);

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	// function to generate access token
	public AuthorizationResponseDto authorizationService() {
		Map<String, String> systemConfigDetails = null;
		Gson gson = new Gson();

		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		AuthorizationResponseDto authorizationResponse = new AuthorizationResponseDto();
		HttpsConnectionResponse httpsConResult = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			systemConfigDetails = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
			if (systemConfigDetails == null) {
				throw new Exception("No config Details Found");
			}
			AuthorizationRequestDto authorizationRequest = new AuthorizationRequestDto();
			authorizationRequest.setClient_secret(systemConfigDetails.get(CommonConstant.CLIENT_SECRET));
			authorizationRequest.setClient_id(systemConfigDetails.get(CommonConstant.CLIENT_ID));
			authorizationRequest.setGrant_type(systemConfigDetails.get(CommonConstant.GRANT_TYPE));
			authorizationRequest.setAudience(systemConfigDetails.get(CommonConstant.AUDIENCE));
			String authRequest = gson.toJson(authorizationRequest);

			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
			connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.AUTH_URL));

			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(authRequest, connectionRequest);
			String serviceResponse = httpsConResult.getResponseData();
			int responseCode = httpsConResult.getRespCode();

			// check success and fail response
			if (responseCode == ReloadlyTopupCodes.S200.getCode()) {
				// convert json to object
				authorizationResponse = (AuthorizationResponseDto) JSONToObjectConversion
						.getObjectFromJson(serviceResponse, AuthorizationResponseDto.class);
				LOGGER.info(
						"AuthorizationServiceImpl in authorizationService function response " + authorizationResponse);
				// log auth response
				logResponse(authorizationResponse);
			} else {
				authorizationResponse.setCode(httpsConResult.getCode());
				authorizationResponse.setMessage(httpsConResult.getTxMessage());
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AuthorizationServiceImpl" + de);
			authorizationResponse.setCode(de.getStatus().getStatusCode());
			authorizationResponse.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthorizationServiceImpl" + e);
			authorizationResponse.setCode(ResponseCodes.ERROR_CODE.getCode());
			authorizationResponse.setMessage(ResponseCodes.ERROR_CODE.getMessage());
		}

		return authorizationResponse;
	}

	// function to log auth response in DB
	private void logResponse(AuthorizationResponseDto response) throws DaoException {

		ReloadlyAuthorizationModel auth = new ReloadlyAuthorizationModel();
		auth.setAccessToken(response.getAccess_token());
		auth.setTokenType(response.getToken_type());
		auth.setExpiresIn(response.getExpires_in());
		auth.setScope(response.getScope());
		auth.setDateLogged(new Date());
		transactionDao.save(auth);
	}
}
