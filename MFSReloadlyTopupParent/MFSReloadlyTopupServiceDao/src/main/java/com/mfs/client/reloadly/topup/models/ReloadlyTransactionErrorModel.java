package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_transaction_error")
public class ReloadlyTransactionErrorModel {

	@Id
	@GeneratedValue
	@Column(name = "tranaction_error_id")
	private int tranactionErrorId;

	@Column(name = "reciepient_country_code")
	private String reciepientCountryCode;

	@Column(name = "reciepient_phone_number")
	private String reciepientPhoneNumber;

	@Column(name = "sender_country_code")
	private String senderCountryCode;

	@Column(name = "sender_number")
	private String senderPhoneNumber;

	@Column(name = "operator_id")
	private int operatorId;

	@Column(name = "custom_identifier")
	private String customIdentifier;

	@Column(name = "amount")
	private double amount;

	@Column(name = "date_logged")
	private Date dateLogged;

	@Column(name = "status")
	private String status;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "code")
	private String code;

	public int getTranactionErrorId() {
		return tranactionErrorId;
	}

	public void setTranactionErrorId(int tranactionErrorId) {
		this.tranactionErrorId = tranactionErrorId;
	}

	public String getReciepientCountryCode() {
		return reciepientCountryCode;
	}

	public void setReciepientCountryCode(String reciepientCountryCode) {
		this.reciepientCountryCode = reciepientCountryCode;
	}

	public String getReciepientPhoneNumber() {
		return reciepientPhoneNumber;
	}

	public void setReciepientPhoneNumber(String reciepientPhoneNumber) {
		this.reciepientPhoneNumber = reciepientPhoneNumber;
	}

	public String getSenderCountryCode() {
		return senderCountryCode;
	}

	public void setSenderCountryCode(String senderCountryCode) {
		this.senderCountryCode = senderCountryCode;
	}

	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}

	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getCustomIdentifier() {
		return customIdentifier;
	}

	public void setCustomIdentifier(String customIdentifier) {
		this.customIdentifier = customIdentifier;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "ReloadlyTransactionErrorModel [tranactionErrorId=" + tranactionErrorId + ", reciepientCountryCode="
				+ reciepientCountryCode + ", reciepientPhoneNumber=" + reciepientPhoneNumber + ", senderCountryCode="
				+ senderCountryCode + ", senderPhoneNumber=" + senderPhoneNumber + ", operatorId=" + operatorId
				+ ", customIdentifier=" + customIdentifier + ", amount=" + amount + ", dateLogged=" + dateLogged
				+ ", status=" + status + ", mfsTransId=" + mfsTransId + ", code=" + code + "]";
	}

}