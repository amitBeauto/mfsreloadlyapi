package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class AccountCommissionByOperatorIdResponseDto 
{
	private String internationalPercentage;
	private String localPercentage;
	private String updatedAt;
	private OperatorResponseDto operator;
	
	private String percentage;
	private String code;
	private String message;
	
	
	public String getInternationalPercentage() {
		return internationalPercentage;
	}
	public void setInternationalPercentage(String internationalPercentage) {
		this.internationalPercentage = internationalPercentage;
	}
	public String getLocalPercentage() {
		return localPercentage;
	}
	public void setLocalPercentage(String localPercentage) {
		this.localPercentage = localPercentage;
	}
	public OperatorResponseDto getOperator() {
		return operator;
	}
	public void setOperator(OperatorResponseDto operator) {
		this.operator = operator;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "AccountCommissionByOperatorIdResponseDto [internationalPercentage=" + internationalPercentage
				+ ", localPercentage=" + localPercentage + ", operator=" + operator + ", updatedAt=" + updatedAt
				+ ", percentage=" + percentage + ", code=" + code + ", message=" + message + "]";
	}
	
	
	
	
}
