package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author priyanka p.patil GetListAllOperatorsResponseDto.java 24-Jan-2020
 *
 *         purpose : this class is used for GetListAllOperators response
 */

@JsonInclude(Include.NON_DEFAULT)
public class GetListAllOperatorsResponseDto {

	private List<ContentResponseDto> content;
	private PageableResponseDto pageable;
	private int totalElements;
	private int totalPages;
	private Boolean last;
	private boolean first;
	private SortResponseDto sort;
	private int numberOfElements;
	private int size;
	private Integer number;
	private Boolean empty;
	
	private String code;
	private String message;
	public List<ContentResponseDto> getContent() {
		return content;
	}
	public void setContent(List<ContentResponseDto> content) {
		this.content = content;
	}
	public PageableResponseDto getPageable() {
		return pageable;
	}
	public void setPageable(PageableResponseDto pageable) {
		this.pageable = pageable;
	}
	public int getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public Boolean getLast() {
		return last;
	}
	public void setLast(Boolean last) {
		this.last = last;
	}
	public boolean isFirst() {
		return first;
	}
	public void setFirst(boolean first) {
		this.first = first;
	}
	public SortResponseDto getSort() {
		return sort;
	}
	public void setSort(SortResponseDto sort) {
		this.sort = sort;
	}
	public int getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Boolean getEmpty() {
		return empty;
	}
	public void setEmpty(Boolean empty) {
		this.empty = empty;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "GetListAllOperatorsResponseDto [content=" + content + ", pageable=" + pageable + ", totalElements="
				+ totalElements + ", totalPages=" + totalPages + ", last=" + last + ", first=" + first + ", sort="
				+ sort + ", numberOfElements=" + numberOfElements + ", size=" + size + ", number=" + number + ", empty="
				+ empty + ", code=" + code + ", message=" + message + "]";
	}
	
}