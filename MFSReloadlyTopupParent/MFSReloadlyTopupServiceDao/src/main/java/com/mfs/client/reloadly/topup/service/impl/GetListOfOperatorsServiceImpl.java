package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponseDto;
import com.mfs.client.reloadly.topup.dto.GetListAllOperatorsResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyContentModel;
import com.mfs.client.reloadly.topup.service.GetListOfOperatorsService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @author priyanka p.patil List All Operator Service.java 24-01-2020
 * 
 *         purpose: this class is used for service List Of Operators
 * 
 *         Methods 1. getListOfOperatorsService 2. logResponse
 */
@Service("GetListOfOperatorsService")
public class GetListOfOperatorsServiceImpl implements GetListOfOperatorsService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(GetListOfOperatorsServiceImpl.class);

	// prepare Get List All Operators Service
	public GetListAllOperatorsResponseDto getListOfOperatorsService() {
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = null;
		GetListAllOperatorsResponseDto responseDto = new GetListAllOperatorsResponseDto();
		String request = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			// get last generated Token ID
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();
			if (accessTokenModel == null) {
				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = accessTokenModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// get Config Details
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (configMap == null) {
						throw new Exception("No config Details Found");
					}

					// set service URL
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(
							configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.OPERATORS));

					// set Headers
					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.AUTHORIZATION,
							CommonConstant.BEARER + accessTokenModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);

					// getting Reloadly Response
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
						// convert that JSON response to Object and Object response to JSON
						responseDto = (GetListAllOperatorsResponseDto) JSONToObjectConversion
								.getObjectFromJson(str_result, GetListAllOperatorsResponseDto.class);
						LOGGER.info(
								"GetListOfOperatorsServiceImpl in GetListOfOperatorsService function " + responseDto);
						// Log Response
						logResponse(responseDto);
					} else {
						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetListOfOperatorsServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in GetListOfOperatorsServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}

	private void logResponse(GetListAllOperatorsResponseDto responseDto) throws DaoException {
		List<ContentResponseDto> contentListData = null;

		contentListData = responseDto.getContent();
		if (contentListData.size() != 0) {
			for (ContentResponseDto contentList : contentListData) {

				String operatorid = contentList.getOperatorId();
				if (null != operatorid) {
					ReloadlyContentModel contentModel = transactionDao.getByOperatorId(operatorid);
					if (null != contentModel) {
						contentModel.setOperatorId(contentList.getOperatorId());
						contentModel.setName(contentList.getName());
						contentModel.setDenominationType(contentList.getDenominationType());
						contentModel.setSenderCurrencyCode(contentList.getSenderCurrencyCode());
						contentModel.setSenderCurrencySymbol(contentList.getSenderCurrencySymbol());
						contentModel.setCommission(contentList.getCommission());
						contentModel.setInternationalDiscount(contentList.getInternationalDiscount());
						contentModel.setLocalDiscount(contentList.getLocalDiscount());
						contentModel.setMostPopularAmount(contentList.getMostPopularAmount());
						contentModel.setDestinationCurrencyCode(contentList.getDestinationCurrencyCode());
						contentModel.setDestinationCurrencySymbol(contentList.getDestinationCurrencySymbol());
						contentModel.setIsoName(contentList.getCountry().getIsoName());
						contentModel.setName(contentList.getCountry().getName());
						contentModel.setDateLogged(new Date());

						String suggestionAmountsList = contentList.getSuggestedAmounts().toString();
						String suggestionAmt = suggestionAmountsList.substring(1, suggestionAmountsList.length() - 1);
						contentModel.setSuggestedAmounts(suggestionAmt);
						
						String fixedAmountsList = contentList.getFixedAmounts().toString();
						String fixedAmt = fixedAmountsList.substring(1, fixedAmountsList.length() - 1);
						contentModel.setFixedAmounts(fixedAmt);

						String suggestedAmountsMap = contentList.getSuggestedAmountsMap().toString();
						String suggesteAmtMap = suggestedAmountsMap.substring(1, suggestedAmountsMap.length() - 1);
						contentModel.setSuggestedAmountsMap(suggesteAmtMap);

						String logoUrls = contentList.getLogoUrls().toString();
						String urls = logoUrls.substring(1, logoUrls.length() - 1);
						contentModel.setLogoUrls(urls);

						contentModel.setMinAmount(contentList.getMinAmount());
						contentModel.setMaxAmount(contentList.getMaxAmount());
						transactionDao.update(contentModel);
					} else {
						ReloadlyContentModel content = new ReloadlyContentModel();

						content.setOperatorId(contentList.getOperatorId());
						content.setName(contentList.getName());
						content.setDenominationType(contentList.getDenominationType());
						content.setSenderCurrencyCode(contentList.getSenderCurrencyCode());
						content.setSenderCurrencySymbol(contentList.getSenderCurrencySymbol());
						content.setCommission(contentList.getCommission());
						content.setInternationalDiscount(contentList.getInternationalDiscount());
						content.setLocalDiscount(contentList.getLocalDiscount());
						content.setMostPopularAmount(contentList.getMostPopularAmount());
						content.setDestinationCurrencyCode(contentList.getDestinationCurrencyCode());
						content.setDestinationCurrencySymbol(contentList.getDestinationCurrencySymbol());
						content.setDateLogged(new Date());
						
						content.setIsoName(contentList.getCountry().getIsoName());
						content.setName(contentList.getCountry().getName());

						String suggestionAmountsList = contentList.getSuggestedAmounts().toString();
						String suggestionAmt = suggestionAmountsList.substring(1, suggestionAmountsList.length() - 1);
						content.setSuggestedAmounts(suggestionAmt);

						String fixedAmountsList = contentList.getFixedAmounts().toString();
						String fixedAmt = fixedAmountsList.substring(1, fixedAmountsList.length() - 1);
						content.setFixedAmounts(fixedAmt);
						
						String suggestedAmountsMap = contentList.getSuggestedAmountsMap().toString();
						String suggesteAmtMap = suggestedAmountsMap.substring(1, suggestedAmountsMap.length() - 1);
						content.setSuggestedAmountsMap(suggesteAmtMap);

						String logoUrls = contentList.getLogoUrls().toString();
						String urls = logoUrls.substring(1, logoUrls.length() - 1);
						content.setLogoUrls(urls);

						content.setMinAmount(contentList.getMinAmount());
						content.setMaxAmount(contentList.getMaxAmount());

						transactionDao.save(content);
					}
				}
			}
		}
	}
}