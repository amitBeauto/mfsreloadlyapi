package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.BalanceResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.BalanceEnquiryService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * @ author Anupriti Prakash Shirke
 * 
 * BalanceEnquiryServiceImpl.java 24-Jan-2020
 * 
 * purpose : this class is used to retrieve Account Balance
 * 
 * Methods 1.balanceService
 * 
 * Change_history : On 03-02-2020 While hitting live URL adding header ->
 * CACHE_CONTROL : no-cache , HOST : topups.reloadly.com
 * 
 */

@Service("BalanceEnquiryService")
public class BalanceEnquiryServiceImpl implements BalanceEnquiryService {

	private static final Logger LOGGER = Logger.getLogger(BalanceEnquiryServiceImpl.class);

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	// function to retrieve Account Balance
	public BalanceResponseDto balanceService() {

		BalanceResponseDto balanceResponseDto = new BalanceResponseDto();
		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		String str_result = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			// To get last generated Token
			ReloadlyAuthorizationModel authModel = transactionDao.getLastRecordByAccessTokenId();
			if (authModel == null) {
				balanceResponseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				balanceResponseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = authModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// To get Config Details
					Map<String, String> systemConfigDetails = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No config Details Found");
					}
					// set service URL
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.ACCOUNT_BALANCE));
					// set Headers
					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.CACHE_CONTROL, CommonConstant.NO_CACHE);
					headerMap.put(CommonConstant.HOST, CommonConstant.TOPUPS_RELOADLY_COM);
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);

					// getting Reloadly Response
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					str_result = httpsConResult.getResponseData();

					// handle success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
						// convert json to object
						balanceResponseDto = (BalanceResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
								BalanceResponseDto.class);
						LOGGER.info("BalanceEnquiryServiceImpl in BalanceEnquiryService function balance response "
								+ balanceResponseDto);
					} else {
						balanceResponseDto.setCode(httpsConResult.getCode());
						balanceResponseDto.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					balanceResponseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					balanceResponseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>Exception in BalanceEnquiryServiceImpl" + de);
			balanceResponseDto.setCode(de.getStatus().getStatusCode());
			balanceResponseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in BalanceEnquiryServiceImpl" + e);
			balanceResponseDto.setCode(ResponseCodes.ER207.getCode());
			balanceResponseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return balanceResponseDto;
	}
}
