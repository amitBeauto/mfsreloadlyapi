package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.dto.ListAllCommissionResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAllOperatorContent;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyCommissionsModel;
import com.mfs.client.reloadly.topup.service.GetListOfAllCommissionService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/*
* @author Pallavi Gohite
* GetListOfAllCommissionService.java
* 11-02-2020
*
* purpose: this class is used for service List Of All Commission
*
* Methods
* 1. listOfAllCommissionService
* 2. logResponse
*
*/

@Service("GetListOfAllCommissionService")
public class GetListOfAllCommissionServiceImpl implements GetListOfAllCommissionService {

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyToupupSystemDao;

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(GetListOfAllCommissionServiceImpl.class);

	public ListAllCommissionResponseDto listOfAllCommissionService(String page, String size) {
		ListAllCommissionResponseDto response = new ListAllCommissionResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			ReloadlyAuthorizationModel authModel = transactionDao.getLastRecordByAccessTokenId();
			if (authModel == null) {
				response.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				response.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = authModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// Get Data from system config
					Map<String, String> systemConfigDetails = reloadlyToupupSystemDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No Config Detail Found");
					}
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.LIST_ALL_COMMISSION_URL) + "?page=" + page
							+ "&size=" + size);
					Map<String, String> headerMap = new HashMap<String, String>();
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());

					connectionRequest.setHeaders(headerMap);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);
					String str_result = httpsConResult.getResponseData();
					int res = httpsConResult.getRespCode();

					// Check for success and fail
					if (res == ReloadlyTopupCodes.S200.getCode()) {

						response = (ListAllCommissionResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
								ListAllCommissionResponseDto.class);
						// log commission response
						logresponse(response);

					} else {
						response.setCode(httpsConResult.getCode());
						response.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					response.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					response.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
				LOGGER.info("GetListOfAllCommissionServiceImpl in List All Commission function response" + response);
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetListOfAllCommissionServiceImpl" + de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in GetListOfAllCommissionServiceImpl" + e);
			response.setCode(ResponseCodes.ER207.getCode());
			response.setMessage(ResponseCodes.ER207.getMessage());
		}
		return response;
	}

	// method for log response
	private void logresponse(ListAllCommissionResponseDto response) throws DaoException {
		List<ListAllOperatorContent> listOperatorContent = null;
		listOperatorContent = response.getContent();

		if (listOperatorContent.size() != 0) {
			for (ListAllOperatorContent contentList : listOperatorContent) {
				String operatorid = contentList.getOperator().getOperatorId();
				ReloadlyCommissionsModel commissionModel = transactionDao.getCommissionByOperatorId(operatorid);
				if (commissionModel == null) {
					commissionModel = new ReloadlyCommissionsModel();
					commissionModel.setContentInternationalPercentage(contentList.getInternationalPercentage());
					commissionModel.setContentLocalPercentage(contentList.getLocalPercentage());
					commissionModel.setContentUpdatedAt(contentList.getUpdatedAt());
					commissionModel.setOperatorOperatorId(contentList.getOperator().getOperatorId());
					commissionModel.setOperatorStatus(contentList.getOperator().getStatus());
					commissionModel.setOperatorBundle(contentList.getOperator().getBundle());

					String promotionIdList = "";

					List<ContentResponse> promotions = contentList.getOperator().getPromotions();
					if (promotions.size() != 0) {
						for (ContentResponse promotion : promotions) {
							promotionIdList += promotion.getPromotionId() + ",";

						}
					}
					commissionModel.setOperatorPromotionsId(promotionIdList);
					commissionModel.setContentPercentage(contentList.getPercentage());
					commissionModel.setDateLogged(new Date());
					transactionDao.save(commissionModel);
				} else {
					commissionModel.setContentInternationalPercentage(contentList.getInternationalPercentage());
					commissionModel.setContentLocalPercentage(contentList.getLocalPercentage());
					commissionModel.setContentUpdatedAt(contentList.getUpdatedAt());
					commissionModel.setOperatorOperatorId(contentList.getOperator().getOperatorId());
					commissionModel.setOperatorStatus(contentList.getOperator().getStatus());
					commissionModel.setOperatorBundle(contentList.getOperator().getBundle());
					String promotionIdList = "";

					List<ContentResponse> promotions = contentList.getOperator().getPromotions();
					if (promotions.size() != 0) {
						for (ContentResponse promotion : promotions) {
							promotionIdList += promotion.getPromotionId() + ",";

						}
					}
					commissionModel.setOperatorPromotionsId(promotionIdList);
					commissionModel.setDateLogged(new Date());
					commissionModel.setContentPercentage(contentList.getPercentage());

					transactionDao.update(commissionModel);
				}
			}
		}

	}

}
