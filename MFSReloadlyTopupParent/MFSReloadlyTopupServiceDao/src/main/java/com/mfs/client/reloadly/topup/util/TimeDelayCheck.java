package com.mfs.client.reloadly.topup.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionLogModel;

@Service
public class TimeDelayCheck {

	@Autowired
	TransactionDao transactionDao;

	// timeDelay function to check time delay for two minutes
	public boolean timeDelay(ReloadlyTransactionLogModel reloadlyTransactionLogModel,
			Map<String, String> systemConfigDetails) throws DaoException, ParseException {
		long diffTime = 0;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = reloadlyTransactionLogModel.getDateLogged();
		String string1 = format.format(startDate);
		Date endDate = new Date();
		String string2 = format.format(endDate);
		Date d1 = format.parse(string1);
		Date d2 = format.parse(string2);

		diffTime = d2.getTime() - d1.getTime();
		long minutes = diffTime / (60 * 1000);
		long seconds = diffTime / 1000 % 60;
		int delayTime = Integer.parseInt(systemConfigDetails.get(CommonConstant.DELAYTIME));
		if (minutes < delayTime) {
			return true;
		}
		return false;
	}
}
