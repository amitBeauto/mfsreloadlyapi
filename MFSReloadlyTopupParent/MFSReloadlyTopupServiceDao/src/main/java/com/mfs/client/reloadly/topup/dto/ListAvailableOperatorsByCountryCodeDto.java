package com.mfs.client.reloadly.topup.dto;

import java.util.List;
import java.util.Map;

public class ListAvailableOperatorsByCountryCodeDto {

	private String operatorId;
	private String name;
	private String bundle;
	private String denominationType;
	private String senderCurrencyCode;
	private String senderCurrencySymbol;
	private String destinationCurrencyCode;
	private String destinationCurrencySymbol;
	private String commission;
	private String internationalDiscount;
	private String localDiscount;
	private String mostPopularAmount;
	private String minAmount;
	private String maxAmount;

	private CountryDto country;
	private List<String> logoUrls;
	private List<String> fixedAmounts;

	private List<String> suggestedAmounts;
	private Map<String, String> suggestedAmountsMap;
	private List<ContentResponse> promotions;

	// getter setter
	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getDenominationType() {
		return denominationType;
	}

	public void setDenominationType(String denominationType) {
		this.denominationType = denominationType;
	}

	public String getSenderCurrencyCode() {
		return senderCurrencyCode;
	}

	public void setSenderCurrencyCode(String senderCurrencyCode) {
		this.senderCurrencyCode = senderCurrencyCode;
	}

	public String getSenderCurrencySymbol() {
		return senderCurrencySymbol;
	}

	public void setSenderCurrencySymbol(String senderCurrencySymbol) {
		this.senderCurrencySymbol = senderCurrencySymbol;
	}

	public String getDestinationCurrencyCode() {
		return destinationCurrencyCode;
	}

	public void setDestinationCurrencyCode(String destinationCurrencyCode) {
		this.destinationCurrencyCode = destinationCurrencyCode;
	}

	public String getDestinationCurrencySymbol() {
		return destinationCurrencySymbol;
	}

	public void setDestinationCurrencySymbol(String destinationCurrencySymbol) {
		this.destinationCurrencySymbol = destinationCurrencySymbol;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getInternationalDiscount() {
		return internationalDiscount;
	}

	public void setInternationalDiscount(String internationalDiscount) {
		this.internationalDiscount = internationalDiscount;
	}

	public String getLocalDiscount() {
		return localDiscount;
	}

	public void setLocalDiscount(String localDiscount) {
		this.localDiscount = localDiscount;
	}

	public String getMostPopularAmount() {
		return mostPopularAmount;
	}

	public void setMostPopularAmount(String mostPopularAmount) {
		this.mostPopularAmount = mostPopularAmount;
	}

	public String getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}

	public String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public CountryDto getCountry() {
		return country;
	}

	public void setCountry(CountryDto country) {
		this.country = country;
	}

	public List<String> getLogoUrls() {
		return logoUrls;
	}

	public void setLogoUrls(List<String> logoUrls) {
		this.logoUrls = logoUrls;
	}

	public List<String> getFixedAmounts() {
		return fixedAmounts;
	}

	public void setFixedAmounts(List<String> fixedAmounts) {
		this.fixedAmounts = fixedAmounts;
	}

	public List<String> getSuggestedAmounts() {
		return suggestedAmounts;
	}

	public void setSuggestedAmounts(List<String> suggestedAmounts) {
		this.suggestedAmounts = suggestedAmounts;
	}

	public Map<String, String> getSuggestedAmountsMap() {
		return suggestedAmountsMap;
	}

	public void setSuggestedAmountsMap(Map<String, String> suggestedAmountsMap) {
		this.suggestedAmountsMap = suggestedAmountsMap;
	}

	public List<ContentResponse> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<ContentResponse> promotions) {
		this.promotions = promotions;
	}

	@Override
	public String toString() {
		return "AllAvailableOperatorByCountryCodeDto [operatorId=" + operatorId + ", name=" + name + ", bundle="
				+ bundle + ", denominationType=" + denominationType + ", senderCurrencyCode=" + senderCurrencyCode
				+ ", senderCurrencySymbol=" + senderCurrencySymbol + ", destinationCurrencyCode="
				+ destinationCurrencyCode + ", destinationCurrencySymbol=" + destinationCurrencySymbol + ", commission="
				+ commission + ", internationalDiscount=" + internationalDiscount + ", localDiscount=" + localDiscount
				+ ", mostPopularAmount=" + mostPopularAmount + ", minAmount=" + minAmount + ", maxAmount=" + maxAmount
				+ ", country=" + country + ", logoUrls=" + logoUrls + ", fixedAmounts=" + fixedAmounts
				+ ", suggestedAmounts=" + suggestedAmounts + ", suggestedAmountsMap=" + suggestedAmountsMap
				+ ", promotions=" + promotions + "]";
	}

}
