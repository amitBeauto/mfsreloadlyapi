package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.GetOperatorByIdService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @author Arnav Gharote
 * 
 *         Get Operator By Id Service.java 24-01-2020
 * 
 *         purpose: this class is used for service Get Operator By Id
 * 
 *         Methods 1. getOperatorById Service
 * 
 */

@Service("GetOperatorByIdService")
public class GetOperatorByIdServiceImpl implements GetOperatorByIdService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(GetOperatorByIdServiceImpl.class);

	public ContentResponseDto getOperatorById(String OperatorId) {

		Map<String, String> header = new HashMap<String, String>();
		ContentResponseDto responseDto = new ContentResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			// get last generated Token ID
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();
			if (accessTokenModel == null) {
				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = accessTokenModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// retrive data from DB
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (configMap == null) {
						throw new Exception("No config details found");
					}
					// set url
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
							+ configMap.get(CommonConstant.OPERATORS) + "/" + OperatorId);
					header.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					header.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					header.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessTokenModel.getAccessToken());
					connectionRequest.setHeaders(header);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					// check for success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
						responseDto = (ContentResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
								ContentResponseDto.class);
						LOGGER.info("GetOperatorByIdServiceImpl in getOperatorById function " + responseDto);
					} else {
						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetOperatorByIdServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in GetOperatorByIdServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}
}
