package com.mfs.client.reloadly.topup.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.models.SystemConfigModel;


@EnableTransactionManagement
@Repository("ReloadlyTopupSystemConfigDao")
@Component
public class ReloadlyTopupSystemConfigDaoImpl implements ReloadlyTopupSystemConfigDao{

	private static final Logger LOGGER = Logger.getLogger(ReloadlyTopupSystemConfigDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public Map<String, String> getConfigDetailsMap() {
		// TODO Auto-generated method stub

		LOGGER.debug("Inside getConfigDetailsMap of ReloadlyTopupSystemConfigDaoImpl");

		Map<String, String> congoSystemConfigurationMap = null;
		Session session = sessionFactory.getCurrentSession();

		String hql = " From SystemConfigModel";
		Query query = session.createQuery(hql);
		List<SystemConfigModel> systemConfigModel = query.list();

		if (systemConfigModel != null && !systemConfigModel.isEmpty()) {
			congoSystemConfigurationMap = new HashMap<String, String>();
			for (SystemConfigModel amCongoSystemConfiguration : systemConfigModel) {
				congoSystemConfigurationMap.put(amCongoSystemConfiguration.getConfigKey(),
						amCongoSystemConfiguration.getConfigValue());
			}
		}
		if (congoSystemConfigurationMap != null && !congoSystemConfigurationMap.isEmpty()) {
			LOGGER.debug("getConfigDetailsMap response size -> " + congoSystemConfigurationMap.size());
		}
		return congoSystemConfigurationMap;

	}

}
