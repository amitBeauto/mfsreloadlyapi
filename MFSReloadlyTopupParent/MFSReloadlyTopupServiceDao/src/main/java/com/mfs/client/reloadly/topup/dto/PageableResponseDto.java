package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author Priyanka Prakash Khade PageableResponseDto.java 25-Jan-2020
 *
 *         purpose : this class is used for Pageable response
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class PageableResponseDto {

	private SortResponseDto sort;

	private Integer pageNumber;

	private int pageSize;

	private Integer offset;

	private Boolean unpaged;

	private Boolean paged;

	public SortResponseDto getSort() {
		return sort;
	}

	public void setSort(SortResponseDto sort) {
		this.sort = sort;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Boolean getUnpaged() {
		return unpaged;
	}

	public void setUnpaged(Boolean unpaged) {
		this.unpaged = unpaged;
	}

	public Boolean getPaged() {
		return paged;
	}

	public void setPaged(Boolean paged) {
		this.paged = paged;
	}

	@Override
	public String toString() {
		return "PageableResponseDto [sort=" + sort + ", pageNumber=" + pageNumber + ", pageSize=" + pageSize
				+ ", offset=" + offset + ", unpaged=" + unpaged + ", paged=" + paged + "]";
	}

}
