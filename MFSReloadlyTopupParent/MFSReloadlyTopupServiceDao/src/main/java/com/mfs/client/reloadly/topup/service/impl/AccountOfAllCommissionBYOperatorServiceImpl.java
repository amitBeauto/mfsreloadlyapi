package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.AccountCommissionByOperatorIdResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.AccountCommissionServiceByOperatorIdService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/*
* @author Pallavi Gohite
* AccountOfAllCommissionBYOperatorService.java
* 11-02-2020
*
* purpose: this class is used for service List Of All Commission by Operator Id
*
* Methods
* 1. listOfAllCommissionService by operator id
*/
@Service("AccountCommissionServiceByOperatorIdService")
public class AccountOfAllCommissionBYOperatorServiceImpl implements AccountCommissionServiceByOperatorIdService {

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyToupupSystemDao;

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(AccountOfAllCommissionBYOperatorServiceImpl.class);

	public AccountCommissionByOperatorIdResponseDto accountCommissionByOperatorId(String opretorId) {
		LOGGER.info(
				"AccountOfAllCommissionBYOperatorServiceImpl in CommissionByOperatorId function request" + opretorId);
		AccountCommissionByOperatorIdResponseDto response = new AccountCommissionByOperatorIdResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			ReloadlyAuthorizationModel authModel = transactionDao.getLastRecordByAccessTokenId();
			if (authModel == null) {
				response.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				response.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = authModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// Get Data from system config
					Map<String, String> systemConfigDetails = reloadlyToupupSystemDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No Config Detail Found");
					}
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.OPERATORS) + "/" + opretorId + "/"
							+ systemConfigDetails.get(CommonConstant.COMMISSIONS));
					Map<String, String> headerMap = new HashMap<String, String>();
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());

					connectionRequest.setHeaders(headerMap);

					
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);
					String str_result = httpsConResult.getResponseData();
					int res = httpsConResult.getRespCode();

					// Check for success and fail
					if (res == ReloadlyTopupCodes.S200.getCode()) {

						response = (AccountCommissionByOperatorIdResponseDto) JSONToObjectConversion
								.getObjectFromJson(str_result, AccountCommissionByOperatorIdResponseDto.class);

					} else {
						response.setCode(httpsConResult.getCode());
						response.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					response.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					response.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
				LOGGER.info(
						"AccountOfAllCommissionBYOperatorServiceImpl in commission by operators Id function response"
								+ response);
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AccountOfAllCommissionBYOperatorServiceImpl" + de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AccountOfAllCommissionBYOperatorServiceImpl" + e);
			response.setCode(ResponseCodes.ER207.getCode());
			response.setMessage(ResponseCodes.ER207.getMessage());
		}
		return response;
	}

}
