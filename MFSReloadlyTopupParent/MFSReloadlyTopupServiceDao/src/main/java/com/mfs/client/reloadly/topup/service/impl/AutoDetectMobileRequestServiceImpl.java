package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileRequestDto;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.AutoDetectMobileRequestService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/*
* @author Pallavi Gohite
* AutoDetectMobileRequestService.java
* 24-01-2020
*
* purpose: this class is used for service Auto-Detect Mobile
*
* Methods
* 1. autoDetectMobile
* 2. logResponse
*
*/

@Service("AutoDetectMobileRequestService")
public class AutoDetectMobileRequestServiceImpl implements AutoDetectMobileRequestService {

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyToupupSystemDao;

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(AutoDetectMobileRequestServiceImpl.class);

	// This method is use to Auto detect mobile operator by phone number
	public AutoDetectMobileResponseDto autoDetectMobile(AutoDetectMobileRequestDto request) {

		LOGGER.info("AutoDetectMobileRequestServiceImpl in autoDetectMobile function request" + request);
		AutoDetectMobileResponseDto response = new AutoDetectMobileResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			ReloadlyAuthorizationModel authModel = transactionDao.getLastRecordByAccessTokenId();
			if (authModel == null) {
				response.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				response.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = authModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// Get Data from system config
					Map<String, String> systemConfigDetails = reloadlyToupupSystemDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No Config Detail Found");
					}
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.AUTO_DETECT_MOBILE_URL) + "/" + "phone/"
							+ request.getPhone() + "/country-code/" + request.getCountryIsoCode());
					Map<String, String> headerMap = new HashMap<String, String>();
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);
					String str_result = httpsConResult.getResponseData();
					int res = httpsConResult.getRespCode();

					// Check for success and fail
					if (res == ReloadlyTopupCodes.S200.getCode()) {

						response = (AutoDetectMobileResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
								AutoDetectMobileResponseDto.class);
					} else {
						response.setCode(httpsConResult.getCode());
						response.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					response.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					response.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
				LOGGER.info("AutoDetectMobileRequestServiceImpl in autoDetectMobile function response" + response);
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AutoDetectMobileRequestServiceImpl" + de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AutoDetectMobileRequestServiceImpl" + e);
			response.setCode(ResponseCodes.COULD_NOT_AUTO_DETECT_OPERATOR.getCode());
			response.setMessage(ResponseCodes.COULD_NOT_AUTO_DETECT_OPERATOR.getMessage());
		}
		return response;
	}

}