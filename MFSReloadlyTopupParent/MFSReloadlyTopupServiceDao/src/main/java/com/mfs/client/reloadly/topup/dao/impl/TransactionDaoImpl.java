package com.mfs.client.reloadly.topup.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;
import com.mfs.client.reloadly.topup.dto.ResponseStatus;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyCommissionsModel;
import com.mfs.client.reloadly.topup.models.ReloadlyContentModel;
import com.mfs.client.reloadly.topup.models.ReloadlyListSupportedCountryModel;
import com.mfs.client.reloadly.topup.models.ReloadlyOperatorsPromotionsModel;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionLogModel;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

@EnableTransactionManagement
@Repository("TransactionDao")
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Transactional
	public ReloadlyAuthorizationModel getLastRecordByAccessTokenId() {
		ReloadlyAuthorizationModel reloadlyAuthorizationModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			reloadlyAuthorizationModel = (ReloadlyAuthorizationModel) session
					.createQuery("From ReloadlyAuthorizationModel order by auth_id  DESC").list().get(0);

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER206.getMessage());
			status.setStatusCode(ResponseCodes.ER206.getCode());

		}
		return reloadlyAuthorizationModel;
	}

	@Transactional
	public ReloadlyContentModel getByOperatorId(String operatorId) throws DaoException {
		ReloadlyContentModel operatorModel = null;
		LOGGER.debug("Inside getByOperatorId of TransactionDaoImpl operatorId is:" + operatorId);

		Session session = sessionFactory.getCurrentSession();
		try {

			operatorModel = (ReloadlyContentModel) session
					.createQuery("From ReloadlyContentModel where operatorId=:operatorId ")
					.setParameter("operatorId", operatorId).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);
		}
		return operatorModel;
	}

	// Get Transaction log detail against mfsTransId
	@Transactional
	public ReloadlyTransactionLogModel getSendtopupByMfsTransId(String mfsTransId) throws DaoException {

		ReloadlyTransactionLogModel reloadlyTransactionLogModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			reloadlyTransactionLogModel = (ReloadlyTransactionLogModel) session
					.createQuery("From ReloadlyTransactionLogModel where mfsTransId=:mfsTransId")
					.setParameter("mfsTransId", mfsTransId).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER211.getMessage());
			status.setStatusCode(ResponseCodes.ER211.getCode());
			throw new DaoException(status);
		}
		return reloadlyTransactionLogModel;

	}

	// Get List Supported Countries Details against Country name
	@Transactional
	public ReloadlyListSupportedCountryModel getListCountryByCountryName(String string) throws DaoException {
		ReloadlyListSupportedCountryModel reloadlyListSupportedCountryModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			reloadlyListSupportedCountryModel = (ReloadlyListSupportedCountryModel) session
					.createQuery("From ReloadlyListSupportedCountryModel where name=:name").setParameter("name", string)
					.uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER212.getMessage());
			status.setStatusCode(ResponseCodes.ER212.getCode());
			throw new DaoException(status);
		}
		return reloadlyListSupportedCountryModel;
	}

	// Get Transaction log detail against reciepienPhoneNumber and senderPhoneNumber
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ReloadlyTransactionLogModel> getLastRecordtransactionLogId(String string, String string2)
			throws DaoException {
		List<ReloadlyTransactionLogModel> reloadlyTransactionLogModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			reloadlyTransactionLogModel = (List<ReloadlyTransactionLogModel>) session.createQuery(
					"From ReloadlyTransactionLogModel where senderPhoneNumber=:senderPhoneNumber and reciepientPhoneNumber=:reciepientPhoneNumber order by tranactionLogId  DESC")
					.setParameter("reciepientPhoneNumber", string).setParameter("senderPhoneNumber", string2).list();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER213.getMessage());
			status.setStatusCode(ResponseCodes.ER213.getCode());
			throw new DaoException(status);
		}
		return reloadlyTransactionLogModel;
	}

	@Transactional
	public ReloadlyOperatorsPromotionsModel getByPromotionId(String promotionId) throws DaoException {
		ReloadlyOperatorsPromotionsModel promotionModel = null;
		LOGGER.debug("Inside getByPromotionId of TransactionDaoImpl promotionId is:" + promotionId);

		Session session = sessionFactory.getCurrentSession();
		try {

			promotionModel = (ReloadlyOperatorsPromotionsModel) session
					.createQuery("From ReloadlyOperatorsPromotionsModel where promotionId=:promotionId ")
					.setParameter("promotionId", promotionId).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);
		}
		return promotionModel;
	}

	@Transactional
	public ReloadlyCommissionsModel getCommissionByOperatorId(String operatorid) throws DaoException {
		ReloadlyCommissionsModel commissionModel = null;
		LOGGER.debug("Inside getcommissionByOperatorid of TransactionDaoImpl promotionId is:" + operatorid);

		Session session = sessionFactory.getCurrentSession();
		try {

			commissionModel = (ReloadlyCommissionsModel) session
					.createQuery("From ReloadlyCommissionsModel where operatorOperatorId=:operatorOperatorId ")
					.setParameter("operatorOperatorId", operatorid).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);
		}
		return commissionModel;
	}

	@Transactional
	public ReloadlyTransactionLogModel getTransLogDataByMfsTransId(String mfsTransId) throws DaoException {

		ReloadlyTransactionLogModel reloadlyTransactionLogModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			reloadlyTransactionLogModel = (ReloadlyTransactionLogModel) session
					.createQuery("From ReloadlyTransactionLogModel where mfsTransId=:mfsTransId")
					.setParameter("mfsTransId", mfsTransId).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER211.getMessage());
			status.setStatusCode(ResponseCodes.ER211.getCode());
			throw new DaoException(status);
		}
		
		return reloadlyTransactionLogModel;

	}

}
