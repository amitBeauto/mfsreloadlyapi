package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SendTopupRequestDto {

	private ReciepientPhone recipientPhone;
	private SenderPhone senderPhone;
	private int operatorId;
	private double amount;
	private String customIdentifier;
	private String mfsTransId;

	public ReciepientPhone getRecipientPhone() {
		return recipientPhone;
	}

	public void setRecipientPhone(ReciepientPhone recipientPhone) {
		this.recipientPhone = recipientPhone;
	}

	public SenderPhone getSenderPhone() {
		return senderPhone;
	}

	public void setSenderPhone(SenderPhone senderPhone) {
		this.senderPhone = senderPhone;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCustomIdentifier() {
		return customIdentifier;
	}

	public void setCustomIdentifier(String customIdentifier) {
		this.customIdentifier = customIdentifier;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	@Override
	public String toString() {
		return "SendTopupRequestDto [recipientPhone=" + recipientPhone + ", senderPhone=" + senderPhone
				+ ", operatorId=" + operatorId + ", amount=" + amount + ", customIdentifier=" + customIdentifier
				+ ", mfsTransId=" + mfsTransId + "]";
	}

}