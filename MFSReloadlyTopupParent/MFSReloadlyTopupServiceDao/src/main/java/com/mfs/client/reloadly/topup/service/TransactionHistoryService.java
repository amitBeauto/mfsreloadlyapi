package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.TransactionHistoryResponseDto;

/**
 * @author Priyanka Prakash Khade TransactionHistoryService.java 25-Jan-2020
 * 
 *         purpose : this Interface is used for Transaction History Service
 * 
 *         Methods 1.transactionHistoryService
 * 
 *         Change_history
 */

public interface TransactionHistoryService {

	TransactionHistoryResponseDto transactionHistoryService();

}
