package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.dto.TopUpOperatorsPromotionsResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyOperatorsPromotionsModel;
import com.mfs.client.reloadly.topup.service.TopupOperatorsPromotionsListService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @authorSrishti ListAllTopupOperatorsPromtionsServiceImpl.java 11-02-2020
 * 
 *                purpose: this class is used to get list of all topUp operators
 *                promotions
 * 
 *                Methods: 1. getListOfTopupOperatorsPromotions 2.
 *                logOperatorsPromotionsResponse
 */
@Service("ListAllTopupOperatorsPromtionsService")
public class TopupOperatorsPromotionsListServiceImpl implements TopupOperatorsPromotionsListService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(TopupOperatorsPromotionsListServiceImpl.class);

	// method for getting list of all topUp operators promotions
	public TopUpOperatorsPromotionsResponseDto getListOfTopupOperatorsPromotions() {

		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = null;
		TopUpOperatorsPromotionsResponseDto responseDto = new TopUpOperatorsPromotionsResponseDto();
		String request = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			// get last generated Token
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();

			// check whether accessTokenModel value is null or not
			if (accessTokenModel == null) {

				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());

			} else {

				Date lastDBDate = accessTokenModel.getDateLogged();

				// Checking for token expiration
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());

				if (flag) {

					// get config details
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();

					// check null value for config data
					if (configMap == null) {
						throw new Exception("No config Details Found");
					}

					// set service URL
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(
							configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.PROMOTIONS));

					// set Headers
					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.AUTHORIZATION,
							CommonConstant.BEARER + accessTokenModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);

					// getting Reloadly Response
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					// check whether response is success or fail
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {

						// convert JSON response to Object and Object response to JSON
						responseDto = (TopUpOperatorsPromotionsResponseDto) JSONToObjectConversion
								.getObjectFromJson(str_result, TopUpOperatorsPromotionsResponseDto.class);

						LOGGER.info(
								"ListAllTopupOperatorsPromtionsServiceImpl in ListAllTopupOperatorsPromotionsService function "
										+ responseDto);

						// Log reloadly response
						logOperatorsPromotionsResponse(responseDto);

					} else {

						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}

				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in ListAllTopupOperatorsPromtionsServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in ListAllTopupOperatorsPromtionsServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}

	// log or update operators promotions reloadly response in DB
	private void logOperatorsPromotionsResponse(TopUpOperatorsPromotionsResponseDto responseDto) throws DaoException {

		List<ContentResponse> contentList = null;

		contentList = responseDto.getContent();

		// check size of content list
		if (contentList.size() != 0) {

			// getting content data from content list
			for (ContentResponse contentData : contentList) {

				String promotionId = contentData.getPromotionId();

				// check whether promotionId is null or not
				if (null != promotionId) {

					ReloadlyOperatorsPromotionsModel operatorsPromotionsModel = transactionDao
							.getByPromotionId(promotionId);

					// check whether operatorsPromotionsModel values are null or not
					if (null != operatorsPromotionsModel) {

						// updating response
						operatorsPromotionsModel.setPromotionId(contentData.getPromotionId());
						operatorsPromotionsModel.setOperatorId(contentData.getOperatorId());
						operatorsPromotionsModel.setTitle(contentData.getTitle());
						operatorsPromotionsModel.setTitle2(contentData.getTitle2());
						operatorsPromotionsModel.setDescription(contentData.getDescription());
						operatorsPromotionsModel.setStartDate(contentData.getStartDate());
						operatorsPromotionsModel.setEndDate(contentData.getEndDate());
						operatorsPromotionsModel.setDenominations(contentData.getDenominations());
						operatorsPromotionsModel.setLocalDenominations(contentData.getLocalDenominations());
						operatorsPromotionsModel.setDateLogged(new Date());

						transactionDao.update(operatorsPromotionsModel);

					} else {

						ReloadlyOperatorsPromotionsModel promotionsModel = new ReloadlyOperatorsPromotionsModel();

						// log response
						promotionsModel.setPromotionId(contentData.getPromotionId());
						promotionsModel.setOperatorId(contentData.getOperatorId());
						promotionsModel.setTitle(contentData.getTitle());
						promotionsModel.setTitle2(contentData.getTitle2());
						promotionsModel.setDescription(contentData.getDescription());
						promotionsModel.setStartDate(contentData.getStartDate());
						promotionsModel.setEndDate(contentData.getEndDate());
						promotionsModel.setDenominations(contentData.getDenominations());
						promotionsModel.setLocalDenominations(contentData.getLocalDenominations());
						promotionsModel.setDateLogged(new Date());

						transactionDao.save(promotionsModel);
					}

				}
			}

		}

	}
}
