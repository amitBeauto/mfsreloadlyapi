package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author Srishti R Deo GetListAllOperatorsResponseDto.java 11-Feb-2020
 *
 *         purpose : this class is used for ListAllTopupOperatorsPromotions
 *         response
 */

@JsonInclude(Include.NON_DEFAULT)
public class TopUpOperatorsPromotionsResponseDto {

	private List<ContentResponse> content;
	private PageableResponseDto pageable;
	private int totalPages;
	private Boolean last;
	private int totalElements;
	private SortResponseDto sort;
	private boolean first;
	private int numberOfElements;
	private int size;
	private Integer number;
	private Boolean empty;

	private String code;
	private String message;

	public List<ContentResponse> getContent() {
		return content;
	}

	public void setContent(List<ContentResponse> content) {
		this.content = content;
	}

	public PageableResponseDto getPageable() {
		return pageable;
	}

	public void setPageable(PageableResponseDto pageable) {
		this.pageable = pageable;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public Boolean getLast() {
		return last;
	}

	public void setLast(Boolean last) {
		this.last = last;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public SortResponseDto getSort() {
		return sort;
	}

	public void setSort(SortResponseDto sort) {
		this.sort = sort;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Boolean getEmpty() {
		return empty;
	}

	public void setEmpty(Boolean empty) {
		this.empty = empty;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "TopUpOperatorsPromotionsResponseDto [content=" + content + ", pageable=" + pageable + ", totalPages="
				+ totalPages + ", last=" + last + ", totalElements=" + totalElements + ", sort=" + sort + ", first="
				+ first + ", numberOfElements=" + numberOfElements + ", size=" + size + ", number=" + number
				+ ", empty=" + empty + ", code=" + code + ", message=" + message + "]";
	}

}
