package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_transaction_log")
public class ReloadlyTransactionLogModel {

	@Id
	@GeneratedValue
	@Column(name = "transaction_log_id")
	private int transactionLogId;

	@Column(name = "reciepient_country_code")
	private String reciepientCountryCode;

	@Column(name = "reciepient_phone_number")
	private String reciepientPhoneNumber;

	@Column(name = "sender_country_code")
	private String senderCountryCode;

	@Column(name = "sender_number")
	private String senderPhoneNumber;

	@Column(name = "operator_id")
	private int operatorId;

	@Column(name = "custom_identifier")
	private String customIdentifier;

	@Column(name = "transaction_id")
	private int transactionId;

	@Column(name = "requested_amount")
	private double requestedAmount;

	@Column(name = "requested_amount_currency_code")
	private String requestedAmountCurrencyCode;

	@Column(name = "delivered_amount")
	private double deliveredAmount;

	@Column(name = "delivered_amount_currency_code")
	private String deliveredAmountCurrencyCode;

	@Column(name = "transaction_date")
	private String transactionDate;

	@Column(name = "request_date_logged")
	private Date dateLogged;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "status")
	private String status;

	@Column(name = "response_date_logged")
	private Date responseDateLogged;

	@Column(name = "operator_transaction_id")
	private String operatorTransactionId;

	@Column(name = "discount")
	private Double discount;

	@Column(name = "discount_currency_code")
	private String discountCurrencyCode;

	@Column(name = "operator_name")
	private String operatorName;

	@Column(name = "pin_detail")
	private String pinDetail;

	public int getTransactionLogId() {
		return transactionLogId;
	}

	public void setTransactionLogId(int transactionLogId) {
		this.transactionLogId = transactionLogId;
	}

	public String getReciepientCountryCode() {
		return reciepientCountryCode;
	}

	public void setReciepientCountryCode(String reciepientCountryCode) {
		this.reciepientCountryCode = reciepientCountryCode;
	}

	public String getReciepientPhoneNumber() {
		return reciepientPhoneNumber;
	}

	public void setReciepientPhoneNumber(String reciepientPhoneNumber) {
		this.reciepientPhoneNumber = reciepientPhoneNumber;
	}

	public String getSenderCountryCode() {
		return senderCountryCode;
	}

	public void setSenderCountryCode(String senderCountryCode) {
		this.senderCountryCode = senderCountryCode;
	}

	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}

	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getCustomIdentifier() {
		return customIdentifier;
	}

	public void setCustomIdentifier(String customIdentifier) {
		this.customIdentifier = customIdentifier;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public String getRequestedAmountCurrencyCode() {
		return requestedAmountCurrencyCode;
	}

	public void setRequestedAmountCurrencyCode(String requestedAmountCurrencyCode) {
		this.requestedAmountCurrencyCode = requestedAmountCurrencyCode;
	}

	public double getDeliveredAmount() {
		return deliveredAmount;
	}

	public void setDeliveredAmount(double deliveredAmount) {
		this.deliveredAmount = deliveredAmount;
	}

	public String getDeliveredAmountCurrencyCode() {
		return deliveredAmountCurrencyCode;
	}

	public void setDeliveredAmountCurrencyCode(String deliveredAmountCurrencyCode) {
		this.deliveredAmountCurrencyCode = deliveredAmountCurrencyCode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getResponseDateLogged() {
		return responseDateLogged;
	}

	public void setResponseDateLogged(Date responseDateLogged) {
		this.responseDateLogged = responseDateLogged;
	}

	public String getOperatorTransactionId() {
		return operatorTransactionId;
	}

	public void setOperatorTransactionId(String operatorTransactionId) {
		this.operatorTransactionId = operatorTransactionId;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getDiscountCurrencyCode() {
		return discountCurrencyCode;
	}

	public void setDiscountCurrencyCode(String discountCurrencyCode) {
		this.discountCurrencyCode = discountCurrencyCode;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getPinDetail() {
		return pinDetail;
	}

	public void setPinDetail(String pinDetail) {
		this.pinDetail = pinDetail;
	}

	@Override
	public String toString() {
		return "ReloadlyTransactionLogModel [transactionLogId=" + transactionLogId + ", reciepientCountryCode="
				+ reciepientCountryCode + ", reciepientPhoneNumber=" + reciepientPhoneNumber + ", senderCountryCode="
				+ senderCountryCode + ", senderPhoneNumber=" + senderPhoneNumber + ", operatorId=" + operatorId
				+ ", customIdentifier=" + customIdentifier + ", transactionId=" + transactionId + ", requestedAmount="
				+ requestedAmount + ", requestedAmountCurrencyCode=" + requestedAmountCurrencyCode
				+ ", deliveredAmount=" + deliveredAmount + ", deliveredAmountCurrencyCode="
				+ deliveredAmountCurrencyCode + ", transactionDate=" + transactionDate + ", dateLogged=" + dateLogged
				+ ", mfsTransId=" + mfsTransId + ", status=" + status + ", responseDateLogged=" + responseDateLogged
				+ ", operatorTransactionId=" + operatorTransactionId + ", discount=" + discount
				+ ", discountCurrencyCode=" + discountCurrencyCode + ", operatorName=" + operatorName + ", pinDetail="
				+ pinDetail + "]";
	}

}