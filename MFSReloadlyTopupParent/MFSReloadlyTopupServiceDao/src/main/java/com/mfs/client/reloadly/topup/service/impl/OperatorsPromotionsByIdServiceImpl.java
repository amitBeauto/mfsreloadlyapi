package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByIdService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @authorSrishti OperatorsPromotionsByIdServiceImpl.java 12-02-2020
 * 
 *                purpose: to retrieve the topup operators promotions by
 *                promotion id
 * 
 *                Methods: 1. getPromotionsById
 * 
 */
@Service("GetTopUpOperatorsPromotionsByIdService")
public class OperatorsPromotionsByIdServiceImpl implements OperatorsPromotionsByIdService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(OperatorsPromotionsByIdServiceImpl.class);

	// method for getting operators promotions by promotion id
	public ContentResponse getPromotionsById(String promotionId) {

		Map<String, String> header = new HashMap<String, String>();
		ContentResponse responseDto = new ContentResponse();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			// get last generated token
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();

			// check null values for access token model
			if (accessTokenModel == null) {

				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());

			} else {

				Date lastDBDate = accessTokenModel.getDateLogged();

				// check for token expiration
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());

				if (flag) {

					// retrieve config data from DB
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();

					// check null value for config data
					if (configMap == null) {
						throw new Exception("No config details found");
					}

					// set url
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
							+ configMap.get(CommonConstant.PROMOTIONS) + "/" + promotionId);

					// set headers
					header.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					header.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					header.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessTokenModel.getAccessToken());
					connectionRequest.setHeaders(header);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					// check for success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
						responseDto = (ContentResponse) JSONToObjectConversion.getObjectFromJson(str_result,
								ContentResponse.class);
						LOGGER.info("OperatorsPromotionsByIdServiceImpl in getPromotionsById function " + responseDto);
					} else {
						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}

				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in OperatorsPromotionsByIdServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in OperatorsPromotionsByIdServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}

}
