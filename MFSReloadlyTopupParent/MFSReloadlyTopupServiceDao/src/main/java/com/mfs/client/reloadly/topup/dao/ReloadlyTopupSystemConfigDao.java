package com.mfs.client.reloadly.topup.dao;

import java.util.Map;

public interface ReloadlyTopupSystemConfigDao {

	public Map<String, String> getConfigDetailsMap();

}
