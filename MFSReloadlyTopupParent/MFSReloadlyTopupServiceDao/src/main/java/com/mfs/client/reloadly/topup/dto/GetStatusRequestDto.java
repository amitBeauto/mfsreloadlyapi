package com.mfs.client.reloadly.topup.dto;

public class GetStatusRequestDto {
	
	private String mfsTransId;	
	
	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	@Override
	public String toString() {
		return "GetStatusRequestDto [mfsTransId=" + mfsTransId + "]";
	}
	
	

}
