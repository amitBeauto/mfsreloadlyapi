package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author Priyanka Prakash Khade TransactionsContentResponseDto.java
 *         25-Jan-2020
 *
 *         purpose : this class is used for transaction content response
 * 
 *         Change_history : On 04-02-2020 While hitting live URL added fields ->
 *         String operatorTransactionId , String operatorName , double discount
 *         , String discountCurrencyCode , String pinDetail
 */

public class TransactionsContentResponseDto {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int transactionId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String operatorTransactionId;

	private String customIdentifier;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String recipientPhone;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String senderPhone;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String countryCode;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int operatorId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String operatorName;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double discount;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String discountCurrencyCode;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double requestedAmount;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String requestedAmountCurrencyCode;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double deliveredAmount;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String deliveredAmountCurrencyCode;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String transactionDate;

	private String pinDetail;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getOperatorTransactionId() {
		return operatorTransactionId;
	}

	public void setOperatorTransactionId(String operatorTransactionId) {
		this.operatorTransactionId = operatorTransactionId;
	}

	public String getCustomIdentifier() {
		return customIdentifier;
	}

	public void setCustomIdentifier(String customIdentifier) {
		this.customIdentifier = customIdentifier;
	}

	public String getRecipientPhone() {
		return recipientPhone;
	}

	public void setRecipientPhone(String recipientPhone) {
		this.recipientPhone = recipientPhone;
	}

	public String getSenderPhone() {
		return senderPhone;
	}

	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getDiscountCurrencyCode() {
		return discountCurrencyCode;
	}

	public void setDiscountCurrencyCode(String discountCurrencyCode) {
		this.discountCurrencyCode = discountCurrencyCode;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public String getRequestedAmountCurrencyCode() {
		return requestedAmountCurrencyCode;
	}

	public void setRequestedAmountCurrencyCode(String requestedAmountCurrencyCode) {
		this.requestedAmountCurrencyCode = requestedAmountCurrencyCode;
	}

	public double getDeliveredAmount() {
		return deliveredAmount;
	}

	public void setDeliveredAmount(double deliveredAmount) {
		this.deliveredAmount = deliveredAmount;
	}

	public String getDeliveredAmountCurrencyCode() {
		return deliveredAmountCurrencyCode;
	}

	public void setDeliveredAmountCurrencyCode(String deliveredAmountCurrencyCode) {
		this.deliveredAmountCurrencyCode = deliveredAmountCurrencyCode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getPinDetail() {
		return pinDetail;
	}

	public void setPinDetail(String pinDetail) {
		this.pinDetail = pinDetail;
	}

	@Override
	public String toString() {
		return "TransactionsContentResponseDto [transactionId=" + transactionId + ", operatorTransactionId="
				+ operatorTransactionId + ", customIdentifier=" + customIdentifier + ", recipientPhone="
				+ recipientPhone + ", senderPhone=" + senderPhone + ", countryCode=" + countryCode + ", operatorId="
				+ operatorId + ", operatorName=" + operatorName + ", discount=" + discount + ", discountCurrencyCode="
				+ discountCurrencyCode + ", requestedAmount=" + requestedAmount + ", requestedAmountCurrencyCode="
				+ requestedAmountCurrencyCode + ", deliveredAmount=" + deliveredAmount
				+ ", deliveredAmountCurrencyCode=" + deliveredAmountCurrencyCode + ", transactionDate="
				+ transactionDate + ", pinDetail=" + pinDetail + "]";
	}

}
