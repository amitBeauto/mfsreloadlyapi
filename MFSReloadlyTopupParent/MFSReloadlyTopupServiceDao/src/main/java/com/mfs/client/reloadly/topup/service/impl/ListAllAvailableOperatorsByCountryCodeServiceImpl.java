package com.mfs.client.reloadly.topup.service.impl;

import java.lang.reflect.Type;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ListAllAvailableOperatorsByCountryCodeResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAvailableOperatorsByCountryCodeDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.ListAllAvailableOperatorsByCountryCodeService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;
/**
* 
* @author Arnav Gharote
* 
*        GetCountryByCountryIsoCodeService.java 11-02-2020
* 
*         purpose: this class is used for service List All Available Operators By Country Code Service
* 
*         Methods 1. getAllAvailableOpreatorsByCountryCode service
* 
*/
@Service("ListAllAvailableOperatorsByCountryCodeService")
public class ListAllAvailableOperatorsByCountryCodeServiceImpl
		implements ListAllAvailableOperatorsByCountryCodeService {
	
	private static final Logger LOGGER = Logger.getLogger(ListAllAvailableOperatorsByCountryCodeServiceImpl.class);


	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	@Autowired               
	TransactionDao transactionDao;

	// This method is use to get all available operators by country code
	public ListAllAvailableOperatorsByCountryCodeResponseDto getAllAvailableOpreatorsByCountryCode(String countryIsoCode) {

		ListAllAvailableOperatorsByCountryCodeResponseDto responseDto = new ListAllAvailableOperatorsByCountryCodeResponseDto();
		Map<String, String> header = new HashMap<String, String>();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		Gson gson = new Gson();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			ReloadlyAuthorizationModel accessTokenModel = transactionDao.getLastRecordByAccessTokenId();
			if (accessTokenModel == null) {
				responseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				responseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = accessTokenModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// Get Data from system config
					Map<String, String> configMap = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (configMap == null) {
						throw new Exception("No Config Detail Found");
					}

					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(
							configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.OPERATORS) + "/"
									+ configMap.get(CommonConstant.COUNTRIES) + "/" + countryIsoCode);

					header.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					header.put(CommonConstant.ACCEPT, configMap.get(CommonConstant.ACCEPT));
					header.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessTokenModel.getAccessToken());
					connectionRequest.setHeaders(header);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();
					// check for success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {
	
						Type collectionType = new TypeToken<List<ListAvailableOperatorsByCountryCodeDto>>(){}.getType();
						List<ListAvailableOperatorsByCountryCodeDto> responseDto1 = gson.fromJson(str_result, collectionType);
						responseDto.setListAvailableOperatorByCountryCode(responseDto1);					
						
						 LOGGER.info("ListAllAvailableOperatorsByCountryCodeServiceImpl in getAllAvailableOpreatorsByCountryCode function " +
						 responseDto);
					} else {
						responseDto.setCode(httpsConResult.getCode());
						responseDto.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					responseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					responseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			 LOGGER.error("==>DaoException in ListAllAvailableOperatorsByCountryCodeServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			 LOGGER.error("==>Exception in ListAllAvailableOperatorsByCountryCodeServiceImpl" + e);
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}
}
