package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.GetListAllOperatorsResponseDto;

public interface GetListOfOperatorsService {

	GetListAllOperatorsResponseDto getListOfOperatorsService();
}
