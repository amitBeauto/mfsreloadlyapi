package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @ author Anupriti Prakash shirke BalanceResponseDto.java 24-Jan-2020
 * 
 * purpose : this class is used for account balance response
 * 
 * Change_history : On 03-02-2020 While hitting live URL added fields -> String
 * currencyName
 */

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class BalanceResponseDto {

	private double balance;
	private String currencyCode;
	private String currencyName;
	private String code;
	private String message;

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "BalanceResponseDto [balance=" + balance + ", currencyCode=" + currencyCode + ", currencyName="
				+ currencyName + ", code=" + code + ", message=" + message + "]";
	}

}
