package com.mfs.client.reloadly.topup.service;

import com.mfs.client.reloadly.topup.dto.BalanceResponseDto;

/**
 * @ author Anupriti Prakash Shirke
 *  BalanceEnquiryService.java 
 *  24-Jan-2020
 * 
 * purpose : this interface is used to retrieve Account Balance
 * 
 * Methods 
 * 1.balanceService
 * 
 * Change_history
 */
public interface BalanceEnquiryService {

	BalanceResponseDto balanceService();
}
