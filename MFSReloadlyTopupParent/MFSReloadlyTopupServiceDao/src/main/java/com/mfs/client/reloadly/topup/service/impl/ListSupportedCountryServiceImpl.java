package com.mfs.client.reloadly.topup.service.impl;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.ListAllSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.dto.ListSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.models.ReloadlyListSupportedCountryModel;
import com.mfs.client.reloadly.topup.service.ListSupportedCountryService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * @ author Dhanashree Ingle ListSupportedCountryServiceImpl.java 30-Jan-2020
 * 
 * purpose : this class is used for get List of supported Countries
 * 
 * Methods 1.getListSupportedCountry Methods 2.logResponse
 * 
 * Change_history
 * 
 * Added Filed : Currency Symbol in response Dto Saved and updated the currency
 * symbol
 * 
 */

@Service("ListSupportedCountryService")
public class ListSupportedCountryServiceImpl implements ListSupportedCountryService {

	private static final Logger LOGGER = Logger.getLogger(SendTopupServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	// Prepare List Supported Country Service and get response
	public ListSupportedCountriesResponseDto getListSupportedCountry() {

		ListSupportedCountriesResponseDto listSupportedCountriesResponseDto = new ListSupportedCountriesResponseDto();
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> systemConfigDetails = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		String request = null;
		Gson gson = new Gson();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		try {
			// Get Last Record Details of Access Token
			ReloadlyAuthorizationModel reloadlyAuthorizationModel = transactionDao.getLastRecordByAccessTokenId();
			if (reloadlyAuthorizationModel == null) {
				listSupportedCountriesResponseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				listSupportedCountriesResponseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = reloadlyAuthorizationModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// Get System Config Details
					systemConfigDetails = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No config Details Found");
					}
					// Set Header Parameters
					Map<String, String> headerMap = new HashMap<String, String>();
					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.AUTHORIZATION,
							CommonConstant.BEARER + reloadlyAuthorizationModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));

					// Set URL for List Supported Country
					String url = (systemConfigDetails.get(CommonConstant.BASE_URL)
							+ (systemConfigDetails.get(CommonConstant.COUNTRIES)));
					connectionRequest.setServiceUrl(url);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					String str_result = httpsConResult.getResponseData();

					// Check Response Code recieved in response is Success or not
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {

						Type collectionType = new TypeToken<List<ListAllSupportedCountriesResponseDto>>() {
						}.getType();

						List<ListAllSupportedCountriesResponseDto> listSupportedCountriesResponseDto12 = gson
								.fromJson(str_result, collectionType);

						listSupportedCountriesResponseDto
								.setListAllSupportedCountriesResponseDto(listSupportedCountriesResponseDto12);

						LOGGER.info("ListSupportedCountryServiceImpl in getListSupportedCountry function "
								+ listSupportedCountriesResponseDto);
						// Log Response for ListSupportedCountries
						logResponse(listSupportedCountriesResponseDto12);
					} else {
						listSupportedCountriesResponseDto.setCode(httpsConResult.getCode());
						listSupportedCountriesResponseDto.setMessage(httpsConResult.getTxMessage());
					}
				} else {
					// listSupportedCountriesResponseDto = new ListSupportedCountriesResponseDto();
					listSupportedCountriesResponseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					listSupportedCountriesResponseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in ListSupportedCountryServiceImpl" + de);

			listSupportedCountriesResponseDto.setCode(de.getStatus().getStatusCode());
			listSupportedCountriesResponseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in ListSupportedCountryServiceImpl" + e);

			listSupportedCountriesResponseDto.setCode(ResponseCodes.E401.getCode());
			listSupportedCountriesResponseDto.setMessage(ResponseCodes.E401.getMessage());
		}
		return listSupportedCountriesResponseDto;
	}

	// Log Response for List Supported Country
	private void logResponse(List<ListAllSupportedCountriesResponseDto> listAllSupportedCountriesResponseDto)
			throws DaoException {

		for (ListAllSupportedCountriesResponseDto listAllSupportedCountries : listAllSupportedCountriesResponseDto) {
			// Get ListSupportedCountry details against country name
			ReloadlyListSupportedCountryModel reloadlyListSupportedCountryModel = transactionDao
					.getListCountryByCountryName(listAllSupportedCountries.getName());

			// Check reloadlyListSupportedCountryModel empty
			if (reloadlyListSupportedCountryModel == null) {
				ReloadlyListSupportedCountryModel reloadlyListSupportedCountryModel1 = new ReloadlyListSupportedCountryModel();
				reloadlyListSupportedCountryModel1.setIsoName(listAllSupportedCountries.getIsoName());
				reloadlyListSupportedCountryModel1.setName(listAllSupportedCountries.getName());
				reloadlyListSupportedCountryModel1.setCurrencyName(listAllSupportedCountries.getCurrencyName());
				reloadlyListSupportedCountryModel1.setFlag(listAllSupportedCountries.getFlag());
				reloadlyListSupportedCountryModel1.setCurrencyCode(listAllSupportedCountries.getCurrencyCode());
				reloadlyListSupportedCountryModel1.setCurrencySymbol(listAllSupportedCountries.getCurrencySymbol());
				reloadlyListSupportedCountryModel1.setDateLogged(new Date());

				String callingCode = listAllSupportedCountries.getCallingCodes().toString();
				String callingCodes = callingCode.substring(1, callingCode.length() - 1);
				reloadlyListSupportedCountryModel1.setCallingCodes(callingCodes);
				transactionDao.save(reloadlyListSupportedCountryModel1);
			} else {

				reloadlyListSupportedCountryModel.setIsoName(listAllSupportedCountries.getIsoName());
				reloadlyListSupportedCountryModel.setName(listAllSupportedCountries.getName());
				reloadlyListSupportedCountryModel.setCurrencyName(listAllSupportedCountries.getCurrencyName());
				reloadlyListSupportedCountryModel.setFlag(listAllSupportedCountries.getFlag());
				reloadlyListSupportedCountryModel.setCurrencyCode(listAllSupportedCountries.getCurrencyCode());
				reloadlyListSupportedCountryModel.setCurrencySymbol(listAllSupportedCountries.getCurrencySymbol());
				reloadlyListSupportedCountryModel.setDateLogged(new Date());

				String callingCode = listAllSupportedCountries.getCallingCodes().toString();
				String callingCodes = callingCode.substring(1, callingCode.length() - 1);
				reloadlyListSupportedCountryModel.setCallingCodes(callingCodes);
				transactionDao.update(reloadlyListSupportedCountryModel);
			}
		}
	}
}
