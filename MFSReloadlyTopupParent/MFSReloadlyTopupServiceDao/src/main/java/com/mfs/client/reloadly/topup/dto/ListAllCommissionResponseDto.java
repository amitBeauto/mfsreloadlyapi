package com.mfs.client.reloadly.topup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class ListAllCommissionResponseDto {
	private List<ListAllOperatorContent> content;
	private PageableResponseDto pageable;
	private String totalPages;
	private String totalElements;
	private String last;
	private SortResponseDto sort;
	private String numberOfElements;
	private String first;
	private String size;
	private String number;
	private String empty;
	private String code;
	private String message;
	
	public List<ListAllOperatorContent> getContent() {
		return content;
	}
	public void setContent(List<ListAllOperatorContent> content) {
		this.content = content;
	}
	public PageableResponseDto getPageable() {
		return pageable;
	}
	public void setPageable(PageableResponseDto pageable) {
		this.pageable = pageable;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
		
	public String getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}
	public String getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(String totalElements) {
		this.totalElements = totalElements;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public SortResponseDto getSort() {
		return sort;
	}
	public void setSort(SortResponseDto sort) {
		this.sort = sort;
	}
	public String getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(String numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getEmpty() {
		return empty;
	}
	public void setEmpty(String empty) {
		this.empty = empty;
	}
	@Override
	public String toString() {
		return "ListAllCommissionResponseDto [content=" + content + ", pageable=" + pageable + ", totalPages="
				+ totalPages + ", totalElements=" + totalElements + ", last=" + last + ", sort=" + sort
				+ ", numberOfElements=" + numberOfElements + ", first=" + first + ", size=" + size + ", number="
				+ number + ", empty=" + empty + ", code=" + code + ", message=" + message + "]";
	}
	
}
