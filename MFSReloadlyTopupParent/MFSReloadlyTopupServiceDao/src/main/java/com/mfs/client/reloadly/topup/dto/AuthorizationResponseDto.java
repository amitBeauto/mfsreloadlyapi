package com.mfs.client.reloadly.topup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @ author Sudarshan B Ghorpade
 *  AuthorizationResponseDto.java 
 *  24-Jan-2020
 * 
 * purpose : this class is used for authorization response
 * 
 * Change_history
 */
@JsonInclude(Include.NON_DEFAULT)
public class AuthorizationResponseDto {

	private String access_token;
	private String scope;
	private String expires_in;
	private String token_type;
	private String code;
	private String message;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "AuthenticationResponseDto [access_token=" + access_token + ", scope=" + scope + ", expires_in="
				+ expires_in + ", token_type=" + token_type + ", code=" + code + ", message=" + message + "]";
	}

}
