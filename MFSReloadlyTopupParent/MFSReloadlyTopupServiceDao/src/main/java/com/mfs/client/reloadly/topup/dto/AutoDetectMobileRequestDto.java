package com.mfs.client.reloadly.topup.dto;

/*
 * @author Pallavi Gohite
 * AutoDetectMobileRequestDto.java
 * 21-01-2020
 * 
 * purpose: this class is used for Auto-Detect Mobile Request Dto
 * 
 * 
 */

public class AutoDetectMobileRequestDto {

	private String phone;
	private String countryIsoCode;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountryIsoCode() {
		return countryIsoCode;
	}

	public void setCountryIsoCode(String countryIsoCode) {
		this.countryIsoCode = countryIsoCode;
	}

	@Override
	public String toString() {
		return "AutoDetectMobileRequestDto [phone=" + phone + ", countryIsoCode=" + countryIsoCode + "]";
	}

}
