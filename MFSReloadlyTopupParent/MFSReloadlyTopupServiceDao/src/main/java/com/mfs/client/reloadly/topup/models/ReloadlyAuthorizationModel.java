package com.mfs.client.reloadly.topup.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reloadly_authorization_token")
public class ReloadlyAuthorizationModel {

	@Id
	@GeneratedValue
	@Column(name = "auth_id")
	private int authId;

	
	@Column(name = "access_token",length = 65535)
	private String accessToken;

	@Column(name = "scope")
	private String scope;
	
	@Column(name = "expires_in")
	private String expiresIn;
	
	@Column(name = "token_type")
	private String tokenType;

	@Column(name = "date_logged")
	private Date dateLogged;

	

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}
	
	@Override
	public String toString() {
		return "AuthorizationModel [authId=" + authId + ", accessToken=" + accessToken + ", scope=" + scope
				+ ", expiresIn=" + expiresIn + ", tokenType=" + tokenType + ", dateLogged=" + dateLogged + "]";
	}

}
