package com.mfs.client.reloadly.topup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;
import com.mfs.client.reloadly.topup.dto.GetStatusResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyTransactionLogModel;
import com.mfs.client.reloadly.topup.service.GetStatusService;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * 
 * @author Arnav Gharote
 * 
 *         GetStatusService.java 19-02-2020
 * 
 *         purpose: this class is used for service get status
 * 
 *         Methods 1. getStatus Service
 * 
 */
@Service("GetStatusService")
public class GetStatusServiceImpl implements GetStatusService {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(GetStatusServiceImpl.class);

	public GetStatusResponseDto getStatus(GetStatusRequestDto request) {

		GetStatusResponseDto response = new GetStatusResponseDto();
		try {
			ReloadlyTransactionLogModel transactionDetails = transactionDao
					.getTransLogDataByMfsTransId(request.getMfsTransId());

			if (transactionDetails != null) {

				response.setStatus(transactionDetails.getStatus());

			} else {

				// No mfs trans id in db
				response.setCode(ResponseCodes.MFS_TRANS_ID_NOT_FOUND.getCode());
				response.setMessage(ResponseCodes.MFS_TRANS_ID_NOT_FOUND.getMessage());
			}
		} catch (Exception e) {

			LOGGER.error("==>Exception in GetStatusServiceImpl" + e);
			response.setCode(ResponseCodes.ER207.getCode());
			response.setMessage(ResponseCodes.ER207.getMessage());
		}
		LOGGER.info("getStatus function response" + response);
		return response;
	}

}
