package com.mfs.client.reloadly.topup.service.impl;

import java.util.Date;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.reloadly.topup.dao.ReloadlyTopupSystemConfigDao;
import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.dto.TransactionHistoryResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyAuthorizationModel;
import com.mfs.client.reloadly.topup.service.TransactionHistoryService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.ExpiredToken;
import com.mfs.client.reloadly.topup.util.HttpsConnectionRequest;
import com.mfs.client.reloadly.topup.util.HttpsConnectionResponse;
import com.mfs.client.reloadly.topup.util.HttpsConnectorImpl;
import com.mfs.client.reloadly.topup.util.JSONToObjectConversion;
import com.mfs.client.reloadly.topup.util.ReloadlyTopupCodes;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

/**
 * @ author Priyanka Prakash Khade TransactionHistoryService.java 25-Jan-2020
 * 
 * purpose : this class is used for Transaction History ServiceImpl
 * 
 * Methods 1.transactionHistoryService
 * 
 * Change_history
 */
@Service("TransactionHistoryService")
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

	private static final Logger LOGGER = Logger.getLogger(TransactionHistoryService.class);

	@Autowired
	ReloadlyTopupSystemConfigDao reloadlyTopupSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	public TransactionHistoryResponseDto transactionHistoryService() {

		TransactionHistoryResponseDto transactionResponseDto = new TransactionHistoryResponseDto();

		HttpsConnectionResponse httpsConResult = null;
		String request = null;
		String str_result = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			// To get last generated Token
			ReloadlyAuthorizationModel authModel = transactionDao.getLastRecordByAccessTokenId();
			if (authModel == null) {
				transactionResponseDto.setCode(ResponseCodes.UNAUTHORIZED_TOKEN.getCode());
				transactionResponseDto.setMessage(ResponseCodes.UNAUTHORIZED_TOKEN.getMessage());
			} else {
				Date lastDBDate = authModel.getDateLogged();
				boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
				if (flag) {
					// To get Config Details
					Map<String, String> systemConfigDetails = reloadlyTopupSystemConfigDao.getConfigDetailsMap();
					if (systemConfigDetails == null) {
						throw new Exception("No config Details Found");
					}

					// set service URL
					connectionRequest.setHttpmethodName("GET");
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.TOPUPS_REPORTS_TRANSACTIONS));

					// set Headers
					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());
					headerMap.put(CommonConstant.ACCEPT, systemConfigDetails.get(CommonConstant.ACCEPT));
					connectionRequest.setHeaders(headerMap);

					// getting Reloadly Response
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
					str_result = httpsConResult.getResponseData();

					// handle success and fail response
					if (httpsConResult.getRespCode() == ReloadlyTopupCodes.S200.getCode()) {

						// convert json to object
						transactionResponseDto = (TransactionHistoryResponseDto) JSONToObjectConversion
								.getObjectFromJson(str_result, TransactionHistoryResponseDto.class);
						LOGGER.info("TransactionHistoryServiceImpl in TransactionHistoryService function " + "response "
								+ transactionResponseDto);
					}
				} else {
					transactionResponseDto.setCode(ResponseCodes.TOKEN_EXPIRED.getCode());
					transactionResponseDto.setMessage(ResponseCodes.TOKEN_EXPIRED.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>Exception in TransactionHistoryServiceImpl" + de);
			transactionResponseDto.setCode(de.getStatus().getStatusCode());
			transactionResponseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionHistoryServiceImpl" + e);
			transactionResponseDto.setCode(ResponseCodes.ER207.getCode());
			transactionResponseDto.setMessage(ResponseCodes.ER207.getMessage());
		}
		return transactionResponseDto;

	}
}
