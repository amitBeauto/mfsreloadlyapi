package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;
import com.mfs.client.reloadly.topup.dto.GetStatusResponseDto;
import com.mfs.client.reloadly.topup.service.GetStatusService;

/**
 * 
 * @author Arnav Gharote GetOperatorsByIdTest.java 24-Jan-2020
 * 
 *         purpose : this class is used for test Get Operator By Id
 * 
 *         Methods 1.getOperatorByIdServiceSuccessTest
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetStatusServiceTest {

	@Autowired
	GetStatusService getStatusService;

	// Test for success
	 @Test
	public void getStatusSuccessTest() {

		GetStatusRequestDto request = new GetStatusRequestDto();
		request.setMfsTransId("1");
		GetStatusResponseDto response = getStatusService.getStatus(request);
		Assert.assertEquals("Pending", response.getStatus());
		System.out.println(response);

	}

	// Test for fail
	@Test
	public void getStatusFailTest() {

		GetStatusRequestDto request = new GetStatusRequestDto();
		request.setMfsTransId("2");
		GetStatusResponseDto response = getStatusService.getStatus(request);
		Assert.assertEquals("No mfs trans id in db", response.getMessage());
		System.out.println(response);

	}
}
