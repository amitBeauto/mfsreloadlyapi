package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.dto.OperatorsPromotionsListByOperatorIdResponse;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByOperatorIdService;

/**
 * 
 * @authorSrishti OperatorsPromotionsByOperatorIdServiceTest.java 14-Feb-2020
 * 
 *                purpose : this class is used for testing the
 *                OperatorsPromotionsByOperatorId Service
 * 
 *                Methods 1.operatorsPromotionsByOperatorIdServiceTest
 *                2.operatorsPromotionsByOperatorIdServiceNotNullTest
 *                3.operatorsPromotionsByOperatorIdServiceFailTest
 *                4.operatorsPromotionsByOperatorIdTokenExpireTest
 *                5.operatorsPromotionsByOperatorIdServiceAuthFailTest
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class OperatorsPromotionsByOperatorIdServiceTest {

	@Autowired
	OperatorsPromotionsByOperatorIdService operatorsPromotionsByOperatorIdService;

	// success test
	@Ignore
	@Test
	public void operatorsPromotionsByOperatorIdServiceTest() {

		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId("128");
		ContentResponse responseDto = response.getContent().get(0);
		Assert.assertEquals("5462", responseDto.getPromotionId());
		System.out.println(response);

	}

	// For NotNull
	@Ignore
	@Test
	public void operatorsPromotionsByOperatorIdServiceNotNullTest() {

		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId("128");
		Assert.assertNotNull(response);
		System.out.println(response);
	}

	// For Fail
	@Ignore
	@Test
	public void operatorsPromotionsByOperatorIdServiceFailTest() {

		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId("1");
		Assert.assertEquals("OPERATOR_NOT_FOUND", response.getCode());
		System.out.println(response);

	}

	// For unauthorized token
	@Ignore
	@Test
	public void operatorsPromotionsByOperatorIdTokenExpireTest() {
		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId("128");
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// Test for token Authentication failure
	@Ignore
	@Test
	public void operatorsPromotionsByOperatorIdServiceAuthFailTest() {

		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId("128");
		Assert.assertEquals("AUTHENTICATION_FAILURE", response.getCode());
		System.out.println(response);
	}

}
