package com.mfs.client.reloadly.topup.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.not;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetOperatorByIdRestAssureTest {

	// Test For Success
	@Ignore
	@Test
	public void getOperatorByIdRestAssuredSuccessTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getOperatorById/173").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("operatorId", is("173"));
	}
 
	// Test For NotNull
	@Ignore
	@Test
	public void getOperatorByIdRestAssuredNotNullTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getOperatorById/173").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("operatorId", not(nullValue()));
	}

	//Test For Fail
	@Ignore
	@Test
	public void balanceEnquiryServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getOperatorById/1737").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("RESOURCE_NOT_FOUND"));
	}
}
