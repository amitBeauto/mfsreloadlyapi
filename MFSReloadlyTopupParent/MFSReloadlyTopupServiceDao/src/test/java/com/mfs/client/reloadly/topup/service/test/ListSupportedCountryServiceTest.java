package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ListAllSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.dto.ListSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.service.ListSupportedCountryService;

/**
 * @ author Mohsin Shaikh ListSupportedCountryServiceTest.java 30-Jan-2020
 * 
 * purpose : this class is used to check ListSupportedCountry
 * 
 * Methods 1.List Supported Country Success Test 2.List Supported Country
 * response not null test
 * 
 * 
 * Change_history
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ListSupportedCountryServiceTest {

	@Autowired
	ListSupportedCountryService listSupportedCountryService;

	
	// Check for List Supported Country Success Test
	@Ignore
	@Test
	public void listSupportedCountrySuccessTest() throws DaoException {
		ListSupportedCountriesResponseDto response = null;
		response = listSupportedCountryService.getListSupportedCountry();
		ListAllSupportedCountriesResponseDto listAllSupportedCountriesResponseDto = response
				.getListAllSupportedCountriesResponseDto().get(0);
		Assert.assertEquals("AF", listAllSupportedCountriesResponseDto.getIsoName());
		System.out.println(response);
	}

	// Check for response recieved is null or not
	@Ignore
	@Test
	public void listSupportedCountryNotNullTest() {

		ListSupportedCountriesResponseDto response = listSupportedCountryService.getListSupportedCountry();
		Assert.assertNotNull(response);
		System.out.println(response);
	} 
	 
	//check for Token Expire Test
	@Ignore
	@Test
	public void listSupportedCountryExpiredTokenTest() {
		ListSupportedCountriesResponseDto response=listSupportedCountryService.getListSupportedCountry();
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}
  
	//check for null access token
	@Ignore
	@Test
	public void listSupportedCountryNullAccessTokenTest() {
		ListSupportedCountriesResponseDto response=listSupportedCountryService.getListSupportedCountry();
		Assert.assertEquals("Null Access Token", response.getMessage());
		System.out.println(response);
		
	}
	
}
