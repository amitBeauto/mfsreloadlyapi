package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.TopUpOperatorsPromotionsResponseDto;
import com.mfs.client.reloadly.topup.service.TopupOperatorsPromotionsListService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class TopUpOperatorsPromotionsListServiceTest {

	@Autowired
	TopupOperatorsPromotionsListService topupOperatorsPromotionsListService;

	// Test for success
	@Ignore
	@Test
	public void OperatorsPromotionListSerivceTest() {

		TopUpOperatorsPromotionsResponseDto response = topupOperatorsPromotionsListService
				.getListOfTopupOperatorsPromotions();
		Assert.assertEquals(3, response.getTotalPages());
		System.out.println(response);
	}

	// Test for token Expire
	@Ignore
	@Test
	public void OperatorsPromotionListServiceTokenExpire() {
		TopUpOperatorsPromotionsResponseDto response = topupOperatorsPromotionsListService
				.getListOfTopupOperatorsPromotions();
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// Test for token Authentication failure
	@Ignore
	@Test
	public void OperatorsPromotionListServiceFailTest() {

		TopUpOperatorsPromotionsResponseDto response = topupOperatorsPromotionsListService
				.getListOfTopupOperatorsPromotions();
		Assert.assertEquals("AUTHENTICATION_FAILURE", response.getCode());
		System.out.println(response);
	}
}
