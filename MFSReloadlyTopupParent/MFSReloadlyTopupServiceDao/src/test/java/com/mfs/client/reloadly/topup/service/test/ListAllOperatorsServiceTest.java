package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.GetListAllOperatorsResponseDto;
import com.mfs.client.reloadly.topup.service.GetListOfOperatorsService;

/**
 * 
 * @author priyanka p.patil ListAllOperatorsTest.java 24-Jan-2020
 * 
 *         purpose : this class is used for test List All Operators
 * 
 *         Methods 1.listAllOperatorsServiceSuccessTest
 * 
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ListAllOperatorsServiceTest {

	@Autowired
	GetListOfOperatorsService getListOfOperatorsService;

	// Test for success
	@Ignore
	@Test
	public void listAllOperatorsServiceSuccessTest() {
		GetListAllOperatorsResponseDto response = getListOfOperatorsService.getListOfOperatorsService();
		Assert.assertEquals(3,response.getTotalPages());
		System.out.println(response);
	}
	
	// Test for token Expire
	@Ignore
	@Test
	public void  listAllOperatorsServiceTokenExpire() {
		GetListAllOperatorsResponseDto response = getListOfOperatorsService.getListOfOperatorsService();
		Assert.assertEquals("Token Expired",response.getMessage());
		System.out.println(response);
	}
	
	// Test for token Authentication failure
	@Ignore
	@Test
	public void listAllOperatorsServiceFailServiceTest() {

		GetListAllOperatorsResponseDto response = getListOfOperatorsService.getListOfOperatorsService();
		Assert.assertEquals("AUTHENTICATION_FAILURE",response.getCode());
		System.out.println(response);
	}
}
