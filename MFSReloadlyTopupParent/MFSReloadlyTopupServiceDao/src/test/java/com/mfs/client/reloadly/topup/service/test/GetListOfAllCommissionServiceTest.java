package com.mfs.client.reloadly.topup.service.test;
/**
 * @ author Pallavi GetListOfAllCommissionServiceTest.java 19-Jan-2020
 * 
 * purpose : this class is used for test All Commission List 
 * 
 * Methods : 1.GetListOfAllCommissionServiceSuccessTest 
 * 2.GetListOfAllCommissionServiceFailAndNullTest
 * 
 * Change_history :
 */
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ListAllCommissionResponseDto;
import com.mfs.client.reloadly.topup.service.GetListOfAllCommissionService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetListOfAllCommissionServiceTest {

	@Autowired
	GetListOfAllCommissionService allCommissionService;

	// for success test
	@Ignore
	@Test
	public void GetListOfAllCommissionServiceSuccessTest() {
		ListAllCommissionResponseDto response = allCommissionService.listOfAllCommissionService("1", "2");
		System.out.println(response);
		Assert.assertEquals("0", response.getNumber());

	}

	// for Fail test
	@Ignore
	@Test
	public void GetListOfAllCommissionServiceFailAndNullTest() {
		ListAllCommissionResponseDto response = allCommissionService.listOfAllCommissionService("1", "");
		System.out.println(response);
		Assert.assertEquals("INVALID_REQUEST", response.getCode());

	}

}
