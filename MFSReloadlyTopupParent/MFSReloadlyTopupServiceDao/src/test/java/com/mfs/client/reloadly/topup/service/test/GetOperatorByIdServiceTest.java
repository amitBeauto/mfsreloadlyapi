package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;

import org.junit.Ignore;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ContentResponseDto;
import com.mfs.client.reloadly.topup.service.GetOperatorByIdService;

/**
 * 
 * @author Arnav Gharote GetOperatorsByIdTest.java 24-Jan-2020
 * 
 *         purpose : this class is used for test Get Operator By Id
 * 
 *         Methods 1.getOperatorByIdServiceSuccessTest
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetOperatorByIdServiceTest {

	@Autowired
	GetOperatorByIdService getOperatorByIdService;

	// For success
	@Ignore
	@Test
	public void getOperatorByIdServiceSuccessTest() {

		ContentResponseDto response = getOperatorByIdService.getOperatorById("173");
		Assert.assertEquals("Digicel Haiti", response.getName());
		System.out.println(response);

	}

	// For NotNull
	@Ignore
	@Test
	public void getOperatorByIdNotNullServiceTest() {

		ContentResponseDto response = getOperatorByIdService.getOperatorById("173");
		Assert.assertNotNull(response);
		System.out.println(response);
	}

	// For Fail
	@Ignore
	@Test
	public void getOperatorByIdFailServiceTest() {

		ContentResponseDto response = getOperatorByIdService.getOperatorById("1777");
		Assert.assertEquals("RESOURCE_NOT_FOUND", response.getCode());
		System.out.println(response);
	}

	// For unauthorized token
	@Ignore
	@Test
	public void getOperatorByIdTokenExpire() {
		ContentResponseDto response = getOperatorByIdService.getOperatorById("173");
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// Test for token Authentication failure
	@Ignore
	@Test
	public void getOperatorByIdServiceFailServiceTest() {

		ContentResponseDto response = getOperatorByIdService.getOperatorById("173");
		Assert.assertEquals("AUTHENTICATION_FAILURE", response.getCode());
		System.out.println(response);
	}

}
