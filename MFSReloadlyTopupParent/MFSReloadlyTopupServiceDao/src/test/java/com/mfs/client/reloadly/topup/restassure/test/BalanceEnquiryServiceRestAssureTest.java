package com.mfs.client.reloadly.topup.restassure.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

import static com.jayway.restassured.RestAssured.given;

/**
 * @ author Anupriti Prakash Shirke BalanceEnquiryServiceTest.java 24-Jan-2020
 * 
 * purpose : this class is used for test account balance(using rest assure)
 * 
 * Methods 1.balanceEnquiryServiceSuccessTest 
 * 2.balanceEnquiryServiceNotNullTest
 * 3.balanceEnquiryServiceFailTest
 * 
 * Change_history
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class BalanceEnquiryServiceRestAssureTest {

	// function to test success
	@Ignore
	@Test
	public void balanceEnquiryServiceSuccessTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getbalance").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("currencyCode", is("USD"));

	}

	// function to test not null
    @Ignore
	@Test
	public void balanceEnquiryServiceNotNullTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getbalance").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("currencyCode", not(nullValue()));
	}

	// function to test fail
	@Ignore
	@Test
	public void balanceEnquiryServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/getbalance").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("RESOURCE_NOT_FOUND"));
	}

}
