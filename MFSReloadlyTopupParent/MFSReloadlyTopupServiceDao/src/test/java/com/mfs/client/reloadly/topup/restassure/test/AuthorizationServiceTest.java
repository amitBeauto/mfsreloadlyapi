package com.mfs.client.reloadly.topup.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;

/**
 * @ author Sudarshan B Ghorpade
 *  AuthorizationServiceTest.java 
 *  24-Jan-2020
 * 
 * purpose : this class is used for test authorization(using rest assure) 
 * 
 * Methods 
 * 1.authorizationServiceTest
 * 2.authorizationServiceNotNullTest
 * 3.authorizationServiceFailTest
 * 
 * Change_history
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationServiceTest {

	//function to test success
	@Ignore
	@Test
	public void authorizationServiceTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSReloadlyTopupWeb/auth")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("token_type", is("Bearer"));
	}

	//function to test not null 
	@Ignore
	@Test
	public void authorizationServiceNotNullTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSReloadlyTopupWeb/auth")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("token_type", not(nullValue()));
	}

	//function to test fail
	@Ignore
	@Test
	public void authorizationServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSReloadlyTopupWeb/auth")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ERROR_CODE"));
	}
}