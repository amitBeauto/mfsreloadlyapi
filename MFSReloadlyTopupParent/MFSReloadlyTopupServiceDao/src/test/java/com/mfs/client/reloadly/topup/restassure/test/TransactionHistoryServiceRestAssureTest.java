package com.mfs.client.reloadly.topup.restassure.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertTrue;
import com.jayway.restassured.http.ContentType;
import com.mfs.client.reloadly.topup.dto.TransactionHistoryResponseDto;

/**
 * @author Priyanka Prakash Khade TransactionHistoryServiceRestAssureTest.java
 *         25-Jan-2020
 * 
 *         purpose : this class is used for test Transaction History (using rest
 *         assure)
 * 
 *         Methods 1.transactionHistoryServiceSuccessTest()
 *         2.transactionHistoryServiceNotNull()
 *         3.transactionHistoryServiceFailTest()
 * 
 *         Change_history
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)

public class TransactionHistoryServiceRestAssureTest {

	
	// function to test success transaction response
	@Ignore
	@Test
	public void transactionHistoryServiceSuccessTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/gettransactionhistory").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("totalPages", is(1));

	}

	// function to test not null
	@Ignore
	@Test
	public void transactionHistoryServiceNotNullTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/gettransactionhistory").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("totalPages", not(nullValue()));
	}

	// Test For Fail
	@Ignore
	@Test
	public void transactionHistoryServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/gettransactionhistory").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("RESOURCE_NOT_FOUND"));
	}

}
