package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.AutoDetectMobileRequestDto;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileResponseDto;
import com.mfs.client.reloadly.topup.service.AutoDetectMobileRequestService;

/*
 * @author Pallavi Gohite
 * AutoDetectMobileRequestServiceTest.java
 * 25-01-2020
 * 
 * purpose: this class is used for Test Auto-Detect Mobile
 * 
 * Methods
 * 1.autoDetectMobileTest
 * 2.autoDetectMobileFailTest
 * 
 * Change_history
 * 2. Add autoDetectMobileFailTest
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AutoDetectMobileRequestServiceTest {

	@Autowired
	AutoDetectMobileRequestService autoDetectMobileRequestService;

	// success test
	@Test
	@Ignore
	public void autoDetectMobileTest() {
		AutoDetectMobileRequestDto requestDto = new AutoDetectMobileRequestDto();
		requestDto.setPhone("27764542693");
		requestDto.setCountryIsoCode("ZA");
		AutoDetectMobileResponseDto responseDto = autoDetectMobileRequestService.autoDetectMobile(requestDto);
		Assert.assertEquals("438", responseDto.getOperatorId());
		// System.out.println(responseDto);
	}

	// fail test
	@Test
	@Ignore
	public void autoDetectMobileFailTest() {
		AutoDetectMobileRequestDto request = new AutoDetectMobileRequestDto();
		request.setPhone("27764542693");
		AutoDetectMobileResponseDto response = autoDetectMobileRequestService.autoDetectMobile(request);
		Assert.assertEquals("INVALID_REQUEST", response.getCode());
		// System.out.println(response);
	}

}