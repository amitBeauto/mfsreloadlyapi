package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.GetCountryByCountryIsoCodeResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAllAvailableOperatorsByCountryCodeResponseDto;
import com.mfs.client.reloadly.topup.service.GetCountryByCountryIsoCodeService;

/**
 * 
 * @author Arnav Gharote 11-02-2020
 * 
 *         purpose: this class is used to test GetCountryByCountryIsoCodeService
 * 
 *         Methods: 1.getCountryByCountryIsoCodeServiceSuccessTest
 *         2.getCountryByCountryIsoCodeServiceFailTest
 *         3.getCountryByCountryIsoCodeServiceExpiredTokenTest
 *         4.getCountryByCountryIsoCodeServiceNullAccessTokenTest
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetCountryByCountryIsoCodeServiceTest {

	@Autowired
	GetCountryByCountryIsoCodeService getCountryByCountryIsoCodeService;

	// For Success
	@Ignore
	@Test
	public void getCountryByCountryIsoCodeServiceSuccessTest() {

		GetCountryByCountryIsoCodeResponseDto response = getCountryByCountryIsoCodeService
				.getCountryByCountryIsoCode("IN");
		Assert.assertEquals("India", response.getName());
		System.out.println(response);
	}

	// For Fail
	@Ignore
	@Test
	public void getCountryByCountryIsoCodeServiceFailTest() {

		GetCountryByCountryIsoCodeResponseDto response = getCountryByCountryIsoCodeService
				.getCountryByCountryIsoCode("AT");
		Assert.assertEquals("RESOURCE_NOT_FOUND", response.getCode());
		System.out.println(response);
	}

	// check for Token Expire Test
	@Ignore
	@Test
	public void getCountryByCountryIsoCodeServiceExpiredTokenTest() {
		GetCountryByCountryIsoCodeResponseDto response = getCountryByCountryIsoCodeService
				.getCountryByCountryIsoCode("IN");
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// check for null access token
	@Ignore
	@Test
	public void getCountryByCountryIsoCodeServiceNullAccessTokenTest() {
		GetCountryByCountryIsoCodeResponseDto response = getCountryByCountryIsoCodeService
				.getCountryByCountryIsoCode("IN");
		Assert.assertEquals("Null Access Token", response.getMessage());
		System.out.println(response);

	}

}
