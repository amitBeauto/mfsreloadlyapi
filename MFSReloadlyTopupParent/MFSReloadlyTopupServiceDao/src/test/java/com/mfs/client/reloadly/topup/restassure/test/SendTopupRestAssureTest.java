package com.mfs.client.reloadly.topup.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.reloadly.topup.dto.ReciepientPhone;
import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SenderPhone;

/**
 * @ author Mohsin Shaikh SendTopupRestAssureTest.java 30-Jan-2020
 * 
 * purpose : this class is used to check send topup
 * 
 * Methods 1.Sendtopup success test 2.Sendtopup Duplicate test 3.Sendtopup
 * Validation test 4.Sendtopup Not Null test 5.Sendtopup Fail test
 *
 * 
 * Change_history
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class SendTopupRestAssureTest {

	// Send Topup Success Test
	@Ignore
	@Test
	public void sendTopupSuccess() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("13");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSReloadlyTopupWeb/sendTopup").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("transactionId", is(286311));
	}

	// Send Topup check duplicate record found against mfstransid test
	@Ignore
	@Test
	public void sendTopuptimeDuplicateTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("13");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSReloadlyTopupWeb/sendTopup ").then()
				.statusCode(equalTo(HttpStatus.OK.value()))
				.body("message", is("Duplicate record found i.e is MfsTransId"));
	}

	// SendTopup Validation test
	@Ignore
	@Test
	public void sendTopupValidationTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSReloadlyTopupWeb/sendTopup ").then()
				.statusCode(equalTo(HttpStatus.OK.value()))
				.body("message", is("Validation Error:Invalid Mfs Trans ID"));
	}

	// SendTopup Not Null test
	@Ignore
	@Test
	public void sendTopupNotNullTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("13");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSReloadlyTopupWeb/sendTopup ").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("transactionId", not(nullValue()));
	}

	// Fail Test
	@Ignore
	@Test
	public void sendTopupFail() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("10");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSReloadlyTopupWeb/sendTopup ").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("message", is(""));
	}

}
