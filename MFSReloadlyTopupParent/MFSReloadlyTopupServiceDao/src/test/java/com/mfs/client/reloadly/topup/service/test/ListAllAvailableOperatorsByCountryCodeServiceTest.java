package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ContentResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAllAvailableOperatorsByCountryCodeResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAvailableOperatorsByCountryCodeDto;
import com.mfs.client.reloadly.topup.dto.ListSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.service.ListAllAvailableOperatorsByCountryCodeService;
/**
 * 
 * @author Arnav Gharote  11-02-2020
 * 
 *                purpose: this class is used to test ListAllAvailableOperatorsByCountryCodeService
 * 
 *                Methods: 1.getAllAvailableOpreatorsByCountryCodeServiceSuccessTest
 *                		   2.getAllAvailableOpreatorsByCountryCodeServiceFailTest
 *                		   3.getAllAvailableOpreatorsByCountryCodeServiceExpiredTokenTest
 *                		   4.getAllAvailableOpreatorsByCountryCodeServiceNullAccessTokenTest
 *                
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ListAllAvailableOperatorsByCountryCodeServiceTest {

	@Autowired
	ListAllAvailableOperatorsByCountryCodeService listAllAvailableOperatorsByCountryCodeService;

	//For success
	//@Ignore
	@Test
	public void getAllAvailableOpreatorsByCountryCodeServiceSuccessTest() {

		ListAllAvailableOperatorsByCountryCodeResponseDto response = listAllAvailableOperatorsByCountryCodeService
				.getAllAvailableOpreatorsByCountryCode("");
		ListAvailableOperatorsByCountryCodeDto response1 = response.getListAvailableOperatorByCountryCode().get(0);
		Assert.assertEquals("Vodafone Maharashtra India", response1.getName());
		System.out.println(response);

	}

	// For Fail
	@Ignore
	@Test
	public void getAllAvailableOpreatorsByCountryCodeServiceFailTest() {

		ListAllAvailableOperatorsByCountryCodeResponseDto response = listAllAvailableOperatorsByCountryCodeService
				.getAllAvailableOpreatorsByCountryCode("null");
		Assert.assertEquals("RESOURCE_NOT_FOUND", response.getCode());
		System.out.println(response);
	}

	// check for Token Expire Test
	@Ignore
	@Test
	public void getAllAvailableOpreatorsByCountryCodeServiceExpiredTokenTest() {
		ListAllAvailableOperatorsByCountryCodeResponseDto response = listAllAvailableOperatorsByCountryCodeService
				.getAllAvailableOpreatorsByCountryCode("IN");
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// check for null access token
	@Ignore
	@Test
	public void getAllAvailableOpreatorsByCountryCodeServiceNullAccessTokenTest() {
		ListAllAvailableOperatorsByCountryCodeResponseDto response = listAllAvailableOperatorsByCountryCodeService
				.getAllAvailableOpreatorsByCountryCode("IN");
		Assert.assertEquals("Null Access Token", response.getMessage());
		System.out.println(response);

	}

}
