package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ReciepientPhone;
import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SendTopupResponseDto;
import com.mfs.client.reloadly.topup.dto.SenderPhone;
import com.mfs.client.reloadly.topup.service.SendTopupService;

/**
 * @ author Mohsin Shaikh SendTopupServiceTest.java 30-Jan-2020
 * 
 * purpose : this class is used for send topup
 * 
 * Methods 1.Sendtopup success test 2.Sendtopup Duplicate test 3.Send topup
 * recently recahrged test 4.Sendtopup Not Null test
 * 
 * 
 * Change_history Date - 7/02/2020 1. Added Test For Access Token Expiry 2.
 * Added Test for Null Access Token
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class SendTopupServiceTest {

	@Autowired
	SendTopupService sendTopupService;

	// Check for Send Topup Success Test
	@Ignore
	@Test
	public void sendTopupSuccessTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("10");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertEquals(286311, response.getTransactionId());
		System.out.println(response);

	}

	// Check for Send Topup Duplicate Test
	@Ignore
	@Test
	public void sendTopupDuplicateTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("10");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertEquals("Duplicate record found i.e is MfsTransId", response.getMessage());
		System.out.println(response);

	}

	// Check for phone rcenetly recharged test , Check before two minutes for same
	// number.
	@Ignore
	@Test
	public void sendTopupPhoneRecentlyRechargedTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("11");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertEquals("Phone recently recharged , Try again after 2 minutes. ", response.getMessage());
		System.out.println(response);

	}

	// Check for Send Top up Null AccessToken
	@Ignore
	@Test
	public void sendTopupNullAccessTokenTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("12");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertEquals("Null Access Token", response.getMessage());
		System.out.println(response);

	}

	// Check for Send Topup Response Not Null Test
	@Ignore
	@Test
	public void sendTopupResponseNotNullTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("13");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertNotNull(response);
		System.out.println(response);

	}

	// Check for Token Expired Test
	@Ignore
	@Test
	public void sendTopupExpiredTokenTest() {

		SendTopupRequestDto request = new SendTopupRequestDto();
		request.setMfsTransId("14");
		ReciepientPhone phone = new ReciepientPhone();
		phone.setNumber("2348036440139");
		phone.setCountryCode("NG");
		request.setRecipientPhone(phone);
		SenderPhone phone2 = new SenderPhone();
		phone2.setNumber("2348036440139");
		phone2.setCountryCode("NG");
		request.setSenderPhone(phone2);
		request.setOperatorId(173);
		request.setAmount(15);
		request.setCustomIdentifier("transaction by john@example.com");

		SendTopupResponseDto response = sendTopupService.sendTopup(request);
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);

	}

}
