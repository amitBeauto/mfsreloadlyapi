package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByIdService;

/**
 * 
 * @authorSrishti OperatorsPromotionsByPromotionIdServiceTest.java 14-Feb-2020
 * 
 *                purpose : this class is used for testing the
 *                OperatorsPromotionsById Service
 * 
 *                Methods 1.operatorsPromotionsByIdServiceTest
 *                2.operatorsPromotionsByIdNotNullServiceTest
 *                3.operatorsPromotionsByIdFailServiceTest
 *                4.operatorsPromotionsByIdTokenExpireTest
 *                5.operatorsPromotionsByIdServiceAuthFailTest
 * 
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class OperatorsPromotionsByPromotionIdServiceTest {

	@Autowired
	OperatorsPromotionsByIdService operatorsPromotionsByIdService;

	// success test
	@Ignore
	@Test
	public void operatorsPromotionsByIdServiceTest() {

		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById("5462");
		Assert.assertEquals("128", response.getOperatorId());
		System.out.println(response);

	}

	// For NotNull
	@Ignore
	@Test
	public void operatorsPromotionsByIdNotNullServiceTest() {

		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById("5462");
		Assert.assertNotNull(response);
		System.out.println(response);
	}

	// For Fail
	//@Ignore
	@Test
	public void operatorsPromotionsByIdFailServiceTest() {

		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById("100000");
		Assert.assertEquals("RESOURCE_NOT_FOUND", response.getCode());
		System.out.println(response);
	}

	// For unauthorized token
	@Ignore
	@Test
	public void operatorsPromotionsByIdTokenExpireTest() {
		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById("5462");
		Assert.assertEquals("Token Expired", response.getMessage());
		System.out.println(response);
	}

	// Test for token Authentication failure
	@Ignore
	@Test
	public void operatorsPromotionsByIdServiceAuthFailTest() {

		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById("5462");
		Assert.assertEquals("AUTHENTICATION_FAILURE", response.getCode());
		System.out.println(response);
	}
}