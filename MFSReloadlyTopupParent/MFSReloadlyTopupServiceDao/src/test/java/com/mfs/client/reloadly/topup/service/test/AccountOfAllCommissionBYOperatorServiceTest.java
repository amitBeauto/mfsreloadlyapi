package com.mfs.client.reloadly.topup.service.test;
/**
 * @ author Pallavi AccountOfAllCommissionBYOperatorServiceTest.java 19-Jan-2020
 * 
 * purpose : this class is used for test All Commission List BY Operator Id
 * 
 * Methods : 1.accountCommissionServiceByOperatorIdServiceTest 
 * 2.accountCommissionServiceByOperatorIdServiceFailTest
 * 
 * Change_history :
 */
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.AccountCommissionByOperatorIdResponseDto;
import com.mfs.client.reloadly.topup.service.AccountCommissionServiceByOperatorIdService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AccountOfAllCommissionBYOperatorServiceTest {

	@Autowired
	AccountCommissionServiceByOperatorIdService accountCommissionBYOperatorService;
	
	
	//Success test
	@Ignore
	@Test
	public void accountCommissionServiceByOperatorIdServiceTest()
	{
		AccountCommissionByOperatorIdResponseDto response=accountCommissionBYOperatorService.accountCommissionByOperatorId("3");
		System.out.println(response);
		Assert.assertEquals("0.0", response.getLocalPercentage());
	}
	
	//fail test
	@Ignore
	@Test
	public void accountCommissionServiceByOperatorIdServiceFailTest()
	{
		AccountCommissionByOperatorIdResponseDto response=accountCommissionBYOperatorService.accountCommissionByOperatorId("0");
		System.out.println(response);
		Assert.assertEquals("RESOURCE_NOT_FOUND", response.getCode());
	}
}
