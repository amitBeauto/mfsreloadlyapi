package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.BalanceResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.service.BalanceEnquiryService;

/**
 * @ author Anupriti Prakash Shirke BalanceEnquiryServiceTest.java 24-Jan-2020
 * 
 * purpose : this class is used for test account balance
 * 
 * Methods 1.balanceEnquirySuccessTest
 * 
 * Change_history : On 07/02/2020 Added two Methods
 * 1.balanceEnquiryServiceExpireTokenTest
 * 2.balanceEnquiryServiceAuthenticationTest
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class BalanceEnquiryServiceTest {

	@Autowired
	BalanceEnquiryService balanceEnquiryService;

	// function to test Success
	@Ignore
	@Test
	public void balanceEnquiryServiceSuccessTest() throws DaoException {

		BalanceResponseDto balanceResponseDto = balanceEnquiryService.balanceService();
		Assert.assertNotNull(balanceResponseDto.getBalance());
		System.out.println(balanceResponseDto);
	}

	// function to test Expire Token
	@Ignore
	@Test
	public void balanceEnquiryServiceExpireTokenTest() throws DaoException {

		BalanceResponseDto balanceResponseDto = balanceEnquiryService.balanceService();
		Assert.assertEquals("Token Expired", balanceResponseDto.getMessage());
		System.out.println(balanceResponseDto);

	}

	// function to test Authentication Token
	@Ignore
	@Test
	public void balanceEnquiryServiceNullTokenTest() throws DaoException {

		BalanceResponseDto balanceResponseDto = balanceEnquiryService.balanceService();
		Assert.assertEquals("Null Access Token", balanceResponseDto.getMessage());
		System.out.println(balanceResponseDto);
	}

}
