package com.mfs.client.reloadly.topup.dao.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dao.TransactionDao;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.models.ReloadlyContentModel;
import com.mfs.client.reloadly.topup.service.GetOperatorByIdService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class TransactionDaoTest {

	@Autowired
	GetOperatorByIdService getOperatorByIdService;
	 
	@Autowired
	TransactionDao transactionDao;
	
	//Test for success
	@Ignore
	@Test
	public void getListOfOperatorSuccessTest() throws DaoException {
		ReloadlyContentModel operatorModel = transactionDao.getByOperatorId("2");
		Assert.assertNotNull(operatorModel);
		System.out.println(operatorModel);
	}
}
