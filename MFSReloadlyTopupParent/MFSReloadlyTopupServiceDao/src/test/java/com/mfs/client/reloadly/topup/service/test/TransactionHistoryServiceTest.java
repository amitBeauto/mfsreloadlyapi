package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.TransactionHistoryResponseDto;
import com.mfs.client.reloadly.topup.exception.DaoException;
import com.mfs.client.reloadly.topup.service.TransactionHistoryService;

/**
 * @author Priyanka Prakash Khade TransactionHistoryServiceTest 25-Jan-2020
 * 
 *         purpose : this class is used for test Transaction History (using
 *         service )
 * 
 *         Methods 1.transactionHistorySuccessTest
 * 
 *         Change_history : On 07/02/2020 Added two Methods
 *         1.transactionHistoryServiceExpireTokenTest
 *         2.transactionHistoryServiceAuthenticationTest
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)

public class TransactionHistoryServiceTest {

	@Autowired
	TransactionHistoryService transactionService;

	// For Success
	@Ignore
	@Test
	public void transactionHistorySuccessTest() throws DaoException {

		TransactionHistoryResponseDto transactionHistoryResponseDto = transactionService.transactionHistoryService();

		Assert.assertNotNull(transactionHistoryResponseDto);
		System.out.println(transactionHistoryResponseDto);
	}

	// function to test Expire Token
	@Ignore
	@Test
	public void transactionHistoryServiceExpireTokenTest() throws DaoException {

		TransactionHistoryResponseDto transactionResponseDto = transactionService.transactionHistoryService();
		// balanceEnquiryService.balanceService();
		Assert.assertEquals("Token Expired", transactionResponseDto.getMessage());
		System.out.println(transactionResponseDto);

	}

	// function to test Authentication Token
	//@Ignore
	@Test
	public void transactionHistoryServiceNullTokenTest() throws DaoException {

		TransactionHistoryResponseDto transactionResponseDto = transactionService.transactionHistoryService();
		Assert.assertEquals("Null Access Token", transactionResponseDto.getMessage());
		System.out.println(transactionResponseDto);

	}

}
