package com.mfs.client.reloadly.topup.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileRequestDto;

/*
 * @author Pallavi Gohite
 * AutoDetectMobileRequestServiceTest.java
 * 27-01-2020
 * 
 * purpose: this class is used for Test Auto-Detect Mobile(using rest assure)
 * 
 * Methods
 * 1. autoDetectMobileTest
 * 
 */

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AutoDetectMobileRequestServiceTest {

	@Test
	@Ignore
	public void autoDetectMobileServiceTest() {

		AutoDetectMobileRequestDto requestDto = new AutoDetectMobileRequestDto();
		requestDto.setPhone("27764542693");
		requestDto.setCountryIsoCode("ZA");

		given().body(requestDto).contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/autodetect").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("operatorId", is("438"));

	}
}
