package com.mfs.client.reloadly.topup.restassure.test;

/**
 * 
 * @author priyanka p.patil
 * listAllOperatorsServiceRestAssureTest.java 
 *  24-Jan-2020
 * 
 * purpose : this class is used for test list All Operators Service
 * 
 * Methods 
 * 1.getlistAllOperatorServiecSuccessTest
 * 
 */
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class listAllOperatorsServiceRestAssureTest {

	//Test for success
	@Ignore
	@Test
	public void getlistAllOperatorServiecSuccessTest() {
		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSReloadlyTopupWeb/operatorsList")
		.then().statusCode(equalTo(HttpStatus.OK.value())).body("totalPages", is(3));
	}

	@Ignore
	@Test
	public void getlistAllOperatorServiecAssuredNotNullTest() {

		given().body("").contentType(ContentType.JSON).when()
				.get("http://localhost:8080/MFSReloadlyTopupWeb/operatorsList").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("totalPages", not(nullValue()));
	}
	
}
