package com.mfs.client.reloadly.topup.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.reloadly.topup.dto.AuthorizationResponseDto;
import com.mfs.client.reloadly.topup.service.AuthorizationService;

/**
 * @ author Sudarshan B Ghorpade AuthorizationServiceTest.java 24-Jan-2020
 * 
 * purpose : this class is used for test authorization
 * 
 * Methods : 1.authorizationServiceTest 2.authorizationServiceNotNullTest
 * 3.authorizationServiceAuthenticationFailureTest
 * 4.authorizationServiceInvalidRequestTest
 * 
 * Change_history :
 * 
 * 1. Add authorizationServiceAuthenticationFailureTest 
 * 2. Add authorizationServiceInvalidRequestTest
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationServiceTest {

	@Autowired
	AuthorizationService authorizationService;

	// function to test success
	@Test
	//@Ignore
	public void authorizationServiceTest() throws Exception {

		AuthorizationResponseDto response = authorizationService.authorizationService();

		Assert.assertEquals("Bearer", response.getToken_type());
		System.out.println(response);
	}

	// function to test not null
	@Test
	@Ignore
	public void authorizationServiceNotNullTest() throws Exception {

		AuthorizationResponseDto response = authorizationService.authorizationService();

		Assert.assertNotEquals("access_token", response.getToken_type());
		// System.out.println(response);
	}

	// function to test AuthenticationFailure
	@Test
	@Ignore
	public void authorizationServiceAuthenticationFailureTest() {

		AuthorizationResponseDto response = authorizationService.authorizationService();
		Assert.assertEquals("AUTHENTICATION_FAILURE", response.getCode());
		System.out.println(response);
	}

	// function to test Invalid Request
	@Test
	@Ignore
	public void authorizationServiceInvalidRequestTest() {
		AuthorizationResponseDto response = authorizationService.authorizationService();
		Assert.assertEquals("INVALID_REQUEST", response.getCode());
		// System.out.println(response);

	}

}