package com.mfs.client.reloadly.topup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.reloadly.topup.dto.AccountCommissionByOperatorIdResponseDto;
import com.mfs.client.reloadly.topup.dto.AuthorizationResponseDto;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileRequestDto;
import com.mfs.client.reloadly.topup.dto.AutoDetectMobileResponseDto;
import com.mfs.client.reloadly.topup.dto.BalanceResponseDto;
import com.mfs.client.reloadly.topup.dto.ContentResponse;
import com.mfs.client.reloadly.topup.dto.ContentResponseDto;
import com.mfs.client.reloadly.topup.dto.GetCountryByCountryIsoCodeResponseDto;
import com.mfs.client.reloadly.topup.dto.GetListAllOperatorsResponseDto;
import com.mfs.client.reloadly.topup.dto.GetStatusRequestDto;
import com.mfs.client.reloadly.topup.dto.GetStatusResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAllAvailableOperatorsByCountryCodeResponseDto;
import com.mfs.client.reloadly.topup.dto.ListAllCommissionResponseDto;
import com.mfs.client.reloadly.topup.dto.ListSupportedCountriesResponseDto;
import com.mfs.client.reloadly.topup.dto.OperatorsPromotionsListByOperatorIdResponse;
import com.mfs.client.reloadly.topup.dto.SendTopupRequestDto;
import com.mfs.client.reloadly.topup.dto.SendTopupResponseDto;
import com.mfs.client.reloadly.topup.dto.TopUpOperatorsPromotionsResponseDto;
import com.mfs.client.reloadly.topup.dto.TransactionHistoryResponseDto;
import com.mfs.client.reloadly.topup.service.AccountCommissionServiceByOperatorIdService;
import com.mfs.client.reloadly.topup.service.AuthorizationService;
import com.mfs.client.reloadly.topup.service.AutoDetectMobileRequestService;
import com.mfs.client.reloadly.topup.service.BalanceEnquiryService;
import com.mfs.client.reloadly.topup.service.GetCountryByCountryIsoCodeService;
import com.mfs.client.reloadly.topup.service.GetListOfAllCommissionService;
import com.mfs.client.reloadly.topup.service.GetListOfOperatorsService;
import com.mfs.client.reloadly.topup.service.GetOperatorByIdService;
import com.mfs.client.reloadly.topup.service.GetStatusService;
import com.mfs.client.reloadly.topup.service.ListAllAvailableOperatorsByCountryCodeService;
import com.mfs.client.reloadly.topup.service.ListSupportedCountryService;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByIdService;
import com.mfs.client.reloadly.topup.service.OperatorsPromotionsByOperatorIdService;
import com.mfs.client.reloadly.topup.service.SendTopupService;
import com.mfs.client.reloadly.topup.service.TopupOperatorsPromotionsListService;
import com.mfs.client.reloadly.topup.service.TransactionHistoryService;
import com.mfs.client.reloadly.topup.util.CommonConstant;
import com.mfs.client.reloadly.topup.util.CommonValidations;
import com.mfs.client.reloadly.topup.util.ResponseCodes;

@RestController
public class ReloadlyTopupController {

	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	AutoDetectMobileRequestService autoDetectMobileRequestService;

	@Autowired
	BalanceEnquiryService balanceEnquiryService;

	@Autowired
	TransactionHistoryService transactionHistoryService;

	@Autowired
	GetOperatorByIdService getOperatorByIdService;

	@Autowired
	SendTopupService sendTopupService;

	@Autowired
	ListSupportedCountryService listSupportedCountryService;

	@Autowired
	GetListOfOperatorsService getListOfOperatorsService;

	@Autowired
	TopupOperatorsPromotionsListService topupOperatorsPromotionsListService;

	@Autowired
	OperatorsPromotionsByIdService operatorsPromotionsByIdService;

	@Autowired
	OperatorsPromotionsByOperatorIdService operatorsPromotionsByOperatorIdService;

	@Autowired
	GetListOfAllCommissionService getListOfAllCommissionService;

	@Autowired
	AccountCommissionServiceByOperatorIdService getAccountCommissionsServiceByOperatorIdService;

	@Autowired
	ListAllAvailableOperatorsByCountryCodeService listAllAvailableOperatorsByCountryCodeService;

	@Autowired
	GetCountryByCountryIsoCodeService getCountryByCountryIsoCodeService;

	@Autowired
	GetStatusService getStatusService;

	// function to generate access token
	@RequestMapping(value = "/auth")
	public AuthorizationResponseDto createAuthToken() {

		AuthorizationResponseDto response = authorizationService.authorizationService();
		return response;

	}

	// Function to do sendtopup
	@RequestMapping(value = "/sendTopup", method = RequestMethod.POST)
	public SendTopupResponseDto sendTopup(@RequestBody SendTopupRequestDto requestDto) {

		CommonValidations commonValidation = new CommonValidations();
		SendTopupResponseDto response = null;
		// Check request null or not
		if (requestDto != null) {
			if (commonValidation.validateStringValues(requestDto.getRecipientPhone().getCountryCode())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(ResponseCodes.VALIDATION_ERROR.getMessage() + ":"
						+ CommonConstant.INVALID_RECIPIENT_COUNTRY_CODE);
			} else if (commonValidation.validateStringValues(requestDto.getMfsTransId())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_MFS_TRANSID);
			} else if (commonValidation.validateStringValues(requestDto.getRecipientPhone().getNumber())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_RECIPIENT_PHONE);
			} else if (commonValidation.validateStringValues(requestDto.getSenderPhone().getNumber())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_SENDER_PHONE);
			} else if (commonValidation.validateStringValues(requestDto.getSenderPhone().getCountryCode())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_SENDER_COUNTRY_CODE);
			} else if (commonValidation.validateIntValues(requestDto.getOperatorId())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_OPERATOR_ID);
			} else if (commonValidation.validateAmountValues(requestDto.getAmount())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_AMOUNT);
			} else if (commonValidation.validateStringValues(requestDto.getCustomIdentifier())) {
				response = new SendTopupResponseDto();
				response.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				response.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_CUSTOMER_IDENTIFIER);
			} else {
				response = sendTopupService.sendTopup(requestDto);
			}
		} else {
			response = new SendTopupResponseDto();
			response.setCode(ResponseCodes.ER209.getCode());
			response.setMessage(ResponseCodes.ER209.getMessage());
		}
		return response;

	}

	// Function to get list supported Countries
	@RequestMapping(value = "/getListSupportedCountries", method = RequestMethod.GET)
	public ListSupportedCountriesResponseDto getListSupportedCountries() {

		ListSupportedCountriesResponseDto listAllSupportedCountriesResponseDto = listSupportedCountryService
				.getListSupportedCountry();

		return listAllSupportedCountriesResponseDto;

	}

	@PostMapping(value = "/autodetect")
	public AutoDetectMobileResponseDto autoDetectMobile(@RequestBody AutoDetectMobileRequestDto request) {

		AutoDetectMobileResponseDto response = null;

		CommonValidations commonValidations = new CommonValidations();

		if (request == null) {
			response = new AutoDetectMobileResponseDto();
			response.setCode(ResponseCodes.COULD_NOT_AUTO_DETECT_OPERATOR.getCode());
			response.setMessage(ResponseCodes.COULD_NOT_AUTO_DETECT_OPERATOR.getMessage());

		} else if (commonValidations.validateStringValues(request.getPhone())) {
			response = new AutoDetectMobileResponseDto();
			response.setCode(ResponseCodes.REQUIRED_PHONE_NUMBER.getCode());
			response.setMessage(ResponseCodes.REQUIRED_PHONE_NUMBER.getMessage());

		} else if (commonValidations.validateStringValues(request.getCountryIsoCode())) {
			response = new AutoDetectMobileResponseDto();
			response.setCode(ResponseCodes.REQUIRED_COUNTRY_CODE.getCode());
			response.setMessage(ResponseCodes.REQUIRED_COUNTRY_CODE.getMessage());

		} else {
			response = new AutoDetectMobileResponseDto();
			response = autoDetectMobileRequestService.autoDetectMobile(request);

		}
		return response;
	}

	// function to get operator by id
	@RequestMapping(value = "/getOperatorById/{operatorId}")
	public ContentResponseDto getOperatorById(@PathVariable String operatorId) {

		ContentResponseDto response = getOperatorByIdService.getOperatorById(operatorId);

		return response;
	}

	// function to get list of operators
	@RequestMapping(value = "/operatorsList")
	public GetListAllOperatorsResponseDto getAllOperatorService() {

		GetListAllOperatorsResponseDto response = getListOfOperatorsService.getListOfOperatorsService();

		return response;
	}

	// Functions to retrieve account balance
	@GetMapping(value = "/getbalance")
	public BalanceResponseDto balanceResponse() {
		BalanceResponseDto response = balanceEnquiryService.balanceService();
		return response;
	}

	// Functions to retrieve transaction history
	@GetMapping(value = "/gettransactionhistory")
	public TransactionHistoryResponseDto transactionHistoryresponse() {

		TransactionHistoryResponseDto response = transactionHistoryService.transactionHistoryService();

		return response;

	}

	// function to get list of operators promotions
	@GetMapping(value = "/promotions")
	public TopUpOperatorsPromotionsResponseDto getAllOperatorsPromotionsService() {

		TopUpOperatorsPromotionsResponseDto response = topupOperatorsPromotionsListService
				.getListOfTopupOperatorsPromotions();

		return response;

	}

	// function to retrieve topup operators promotions by promotion id
	@GetMapping(value = "/promotions/{promotionId}")
	public ContentResponse getpromotionsById(@PathVariable String promotionId) {

		ContentResponse response = operatorsPromotionsByIdService.getPromotionsById(promotionId);

		return response;

	}

	// function to retrieve topup operators promotions by operator id
	@GetMapping(value = "/promotions/operators/{operatorId}")
	public OperatorsPromotionsListByOperatorIdResponse getPromotionsByOperatorId(@PathVariable String operatorId) {

		OperatorsPromotionsListByOperatorIdResponse response = operatorsPromotionsByOperatorIdService
				.getPromotionsByOperatorId(operatorId);

		return response;

	}

	// Functions to retrieve List all Commission
	@GetMapping(value = "/commissionslist")
	public ListAllCommissionResponseDto getListAllCommissions(@RequestParam String page, @RequestParam String size) {

		ListAllCommissionResponseDto response = getListOfAllCommissionService.listOfAllCommissionService(page, size);

		return response;

	}

	// Functions to retrieve List all Commission by Id
	@GetMapping(value = "/commissions/{opretorId}")
	public AccountCommissionByOperatorIdResponseDto getListAllCommissionById(@PathVariable String opretorId) {

		AccountCommissionByOperatorIdResponseDto response = getAccountCommissionsServiceByOperatorIdService
				.accountCommissionByOperatorId(opretorId);

		return response;

	}

	// Functions to ListAllAvailableOperatorsByCountryCode
	@GetMapping(value = "/getAllAvailableOpreatorsByCountryCode/{countryIsoCode}")
	public ListAllAvailableOperatorsByCountryCodeResponseDto getAllAvailableOpreatorsByCountryCode(
			@PathVariable String countryIsoCode) {

		ListAllAvailableOperatorsByCountryCodeResponseDto response = listAllAvailableOperatorsByCountryCodeService
				.getAllAvailableOpreatorsByCountryCode(countryIsoCode);

		return response;

	}

	// Functions to getCountryByCountryIsoCode
	@GetMapping(value = "/getCountryByCountryIsoCode/{countryIsoCode}")
	public GetCountryByCountryIsoCodeResponseDto getCountryByCountryIsoCode(@PathVariable String countryIsoCode) {

		GetCountryByCountryIsoCodeResponseDto response = getCountryByCountryIsoCodeService
				.getCountryByCountryIsoCode(countryIsoCode);

		return response;

	}

	// Functions to get status
	@PostMapping(value = "/getStatus")
	public GetStatusResponseDto getStatus(@RequestBody GetStatusRequestDto request) {

		CommonValidations validations = new CommonValidations();
		GetStatusResponseDto response = new GetStatusResponseDto();

		if (validations.validateStringValues(request.getMfsTransId())) {

			response.setCode(ResponseCodes.ER209.getCode());
			response.setMessage(ResponseCodes.ER209.getMessage());

		} else {

			response = getStatusService.getStatus(request);
		}

		return response;

	}

}
